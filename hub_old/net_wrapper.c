#include <malloc.h>

#include "net_wrapper.h"
#include "net/server.h"

void tst() {
    struct ServerData *server_data = server_create(58096);

    int start = server_start(server_data);
    if (start == -1) return;

    server_stop(server_data);
    server_destroy(server_data);
    free(server_data);
}
