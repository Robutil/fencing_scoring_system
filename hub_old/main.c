#include <stdint.h>
#include <stdio.h>
#include "net_wrapper.h"

struct Node {
    uint8_t id;
};
struct Message {
    struct Node *origin;
    unsigned long timestamp;
    uint8_t id;
};

struct Message *awaitMessage() {
    return NULL;
}

enum MsgState {
    Passive,    /* Wait for any Message */
    HitDelay,   /* Received Message, allow other Messages for a short time */
    Active      /* Show result of messages */
};

int main() {
    tst();

    return 0;

    unsigned long long clock = 0;
    unsigned long long switchStateTime = 0;

    enum MsgState msgState = Passive;

    //Connect clients & sync

    while (1) {
        //Check messages
        struct Message *msg = awaitMessage();

        switch (msgState) {
            case Passive:
                msgState = HitDelay;
                switchStateTime = clock + 800;
                break;
            case HitDelay:
                //handleMessage();
                break;

            case Active:
                //handleMessage();
                break;
        }

        //Update clock
        //Update states

        //Delay -> Maybe just use network timeouts
        break;
    }
    return 0;
}