#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "linked_list.h"

struct LinkedList *linked_list_create(void *data, size_t data_length) {
    //Create container
    struct LinkedList *list = malloc(sizeof(struct LinkedList));
    if (!list) {
        perror("[LinkedList](create): Cannot allocate memory for linked list container");
        return NULL;
    }

    //Init fields
    list->parent = NULL;
    list->child = NULL;

    //Copy data
    list->data = malloc(data_length);
    if (!list->data) {
        perror("[LinkedList](create): Cannot allocate memory data");
        free(list);
        return NULL;
    }
    memcpy(list->data, data, data_length);
    list->data_length = data_length;

    return list;
}

void linked_list_destroy(struct LinkedList *link) {
    struct LinkedList *swap;

    //Remove all parents
    struct LinkedList *current_parent = link->parent;
    while (current_parent) {
        //Keep pointer to parent's parent
        swap = current_parent->parent;
        linked_list_remove(current_parent);
        current_parent = swap; //Cont removing

        //Check if we are a circular linked list
        if (current_parent == link) {
            //We have removed all the children because they were also our parents, clean up self now
            linked_list_log(link);
            linked_list_remove(link);
            return;
        }
    }

    //Remove all children (we are not a circular linked list)
    struct LinkedList *children = link->child;
    while (children) {
        swap = children->child;
        linked_list_remove(children);
        children = swap;
    }

    //Finally, remove self
    linked_list_remove(link);
}


int linked_list_insert_after(void *data, size_t data_length, struct LinkedList *link) {
    //TODO: merge code with linked_list_insert_before
    if (link == NULL) {
        printf("[LinkedList](insert_after): Cannot insert after NULL element");
        return -1;
    }

    //Create new container
    struct LinkedList *new_link = linked_list_create(data, data_length);
    if (!new_link) {
        perror("[LinkedList](insert_after): Could not create new link");
        return -1;
    }

    //If the given element had a child, inherit it
    if (link->child != NULL) {
        new_link->child = link->child;
    }

    new_link->parent = link;
    link->child = new_link;

    return 0; //Success
}

int linked_list_insert_before(void *data, size_t data_length, struct LinkedList *link) {
    //TODO: merge code with linked_list_insert_after
    if (link == NULL) {
        printf("[LinkedList](insert_before): Cannot insert after NULL element");
        return -1;
    }

    //Create new container
    struct LinkedList *new_link = linked_list_create(data, data_length);
    if (!new_link) {
        perror("[LinkedList](insert_before): Could not create new link");
        return -1;
    }

    //If the given element had a parent, inherit it
    if (link->parent != NULL) {
        new_link->parent = link->parent;
    }

    new_link->child = link;
    link->parent = new_link;

    return 0; //Success
}

int linked_list_push(void *data, size_t data_length, struct LinkedList *link) {
    return linked_list_insert_after(data, data_length, linked_list_get_last(link));
}

struct LinkedList *linked_list_get_root(struct LinkedList *link) {
    if (link == NULL) return NULL;

    //Keep check if a fixed point to avoid looping
    struct LinkedList *avoid_loop_link = link->child;

    struct LinkedList *first_element = link;
    while (first_element->parent != NULL) {
        first_element = first_element->parent;

        //In case of a circular linked list, abort
        if (avoid_loop_link == first_element) {
            return NULL;
        }
    }
    return first_element;
}

struct LinkedList *linked_list_get_last(struct LinkedList *link) {
    if (link == NULL) return NULL;

    //Keep check if a fixed point to avoid looping
    struct LinkedList *avoid_loop_link = link->parent;

    struct LinkedList *last_element = link;
    while (last_element->child != NULL) {
        last_element = last_element->child;

        //In case of a circular linked list, abort
        if (avoid_loop_link == last_element) {
            return NULL;
        }
    }
    return last_element;
}

void linked_list_remove(struct LinkedList *link) {
    if (link == NULL) return;

    //Let relations inherit
    if (link->parent && link->child) {
        link->parent->child = link->child;
        link->child->parent = link->parent;

    } else if (!link->parent) {
        if (link->child) link->child->parent = NULL;

    } else if (!link->child) {
        if (link->parent) link->parent->child = NULL;
    }

    //Clean up
    free(link->data);
    free(link);
}

int linked_list_make_circular(struct LinkedList *link) {
    if (!link) return -1;

    struct LinkedList *start = linked_list_get_root(link);
    if (!start) return -1;

    struct LinkedList *end = linked_list_get_last(link);
    if (!end) return -1;

    start->parent = end;
    end->child = start;
    return 0;
}

void linked_list_log(struct LinkedList *link) {
    if (!link) {
        printf("[LinkedList](log): %p", link);
    } else {
        printf("[LinkedList](log): Parent: %p Child: %p Me: %p Data: %p\n", link->parent, link->child, link,
               link->data);
    }
}

void linked_list_log_full(struct LinkedList *link) {
    if (!link) {
        printf("[LinkedList](log_full): %p", link);
        return;
    }

    //Find start, in case of circular linked list, use given element
    struct LinkedList *log_link = linked_list_get_root(link);
    if (log_link == NULL) log_link = link;
    struct LinkedList *avoid_loop_link = link;

    int counter = 0;
    while (log_link != NULL) {
        counter++;
        printf("[%d] ", counter);
        linked_list_log(log_link);
        log_link = log_link->child;

        if (log_link == avoid_loop_link) break;
    }
    printf("[LinkedList](log_full): %d elements in the list\n", counter);
}