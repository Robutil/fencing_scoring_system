#ifndef C_LINEAR_BUFFER_H
#define C_LINEAR_BUFFER_H

#include <glob.h> //size_t

/**
 * A LinearBuffer is a dynamically allocated array with pointers to
 * where the data is being written into the buffer and read from.
 * */
struct LinearBuffer {
    void *data;
    size_t size;

    size_t head_pointer;
    size_t tail_pointer;
};

/**
 * Creates a struct LinearBuffer* of given size. This buffer can be
 * used to push and pop bytes. When bytes are popped the buffer will
 * re-use this space. This operation is automatic and is executed when
 * there is not enough space, this (sometimes) requires a memory move.
 *
 * @param size Maximum size of the buffer
 *
 * @return A new struct LinearBuffer* or NULL if memory could not be allocated
 * */
struct LinearBuffer *linear_buffer_create(size_t size);

/**
 * Destroys the given buffer by freeing the data container
 *
 * @param buffer Buffer to destroy
 * */
void linear_buffer_destroy(struct LinearBuffer *buffer);

/**
 * Appends data of given length to the end of the buffer.
 *
 * @param buffer Buffer to append data to
 * @param data Data which to append
 * @param data_length Size of data parameter
 *
 * @return 0 on success or -1 when the given buffer is not large enough
 * */
int linear_buffer_push(struct LinearBuffer *buffer, void *data, size_t data_length);

/**
 * Looks at the tail end of the buffer and returns a pointer to where
 * that data starts
 *
 * @param buffer Buffer to look into
 *
 * @return Pointer to start of data in this buffer
 * */
void *linear_buffer_peek(struct LinearBuffer *buffer);

/**
 * Looks at the tail end of the buffer and returns a newly allocated buffer
 * which contains all available data in the given buffer. The returned data
 * is removed from the given buffer. The length of this data is equal to
 * linear_buffer_size() before calling this function.
 *
 * @param buffer Buffer to look into
 *
 * @return Pointer containing all data previously in buffer
 * */
void *linear_buffer_pop(struct LinearBuffer *buffer);

/**
 * Looks at the tail end of the buffer and returns a newly allocated buffer
 * of given size which contains elements starting at the tail and ending
 * at tail + size. The popped data is removed from the given buffer.
 *
 * @param buffer Buffer to look into
 * @param length Length of data to sliced off
 *
 * @return Pointer of size length containing tail-end data removed from buffer
 * */
void *linear_buffer_pop_bytes(struct LinearBuffer *buffer, size_t length);

/**
 * Calculates the length of the data that is currently unread in the buffer.
 *
 * @param buffer Buffer to calculate length from
 *
 * @return Length of unread data in the given buffer
 * */
size_t linear_buffer_size(struct LinearBuffer *buffer);

/**
 * Calculates how many bytes there are available in the given buffer
 *
 * @param buffer Buffer to check
 *
 * @return Maximum amount of bytes that can be written
 * */
size_t linear_buffer_available(struct LinearBuffer *buffer);

/**
 * With every read and write the tail and head move. This function makes
 * sure that the tail is set to zero by overwriting read memory and thus
 * bringing unread bytes to the front. This allows for optimal use of the
 * buffer space.
 *
 * @param buffer Buffer to clean
 * */
void linear_buffer_clean(struct LinearBuffer *buffer);

/**
 * Resets the tail and head pointers, thus setting the unread data count
 * to zero. This function sets the buffer to its initial state.
 *
 * @param buffer Buffer to reset
 * */
void linear_buffer_reset(struct LinearBuffer *buffer);

/**
 * Prints a human readable representation of the provided buffer. The data itself
 * is not printed.
 *
 * @param buffer Buffer to print
 * */
void linear_buffer_log(struct LinearBuffer *buffer);

#endif //C_LINEAR_BUFFER_H
