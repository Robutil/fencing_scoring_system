#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>

#include "circular_buffer.h"

struct CircularBuffer *circular_buffer_create(size_t number_of_elements, size_t element_size) {
    if (number_of_elements == 0 || element_size == 0) {
        errno = EINVAL;
        perror("[CircularBuffer](create): incorrect elements");
        return NULL;
    }

    struct CircularBuffer *buffer = malloc(sizeof(struct CircularBuffer));
    if (!buffer) {
        perror("[CircularBuffer](create): Could not allocate struct memory");
        return NULL;
    }

    buffer->number_of_elements = number_of_elements;
    buffer->element_size = element_size;
    buffer->head_pointer = 0;
    buffer->tail_pointer = 0;
    buffer->is_empty = true;

    buffer->data = malloc(sizeof(element_size) * number_of_elements);
    if (!buffer->data) {
        perror("[CircularBuffer](create): Could not allocate data memory");
        free(buffer);
        return NULL;
    }

    return buffer;
}

void circular_buffer_destroy(struct CircularBuffer *buffer) {
    if (buffer) free(buffer->data);
    free(buffer);
}

int circular_buffer_push(struct CircularBuffer *buffer, void *data) {
    //Check if we reached maximum capacity
    if (!buffer->is_empty && buffer->head_pointer == buffer->tail_pointer) {
        printf("[CircularBuffer](push): Cannot push data, buffer overflow!\n");
        return -1;
    }

    //Set empty flag if needed (we are storing data, no longer empty)
    if (buffer->is_empty) buffer->is_empty = false;

    //Copy data into our buffer
    memcpy(buffer->data + buffer->head_pointer * buffer->element_size, data, buffer->element_size);

    //Increment head pointer, reset if we reached end of buffer
    buffer->head_pointer++;
    if (buffer->head_pointer >= buffer->number_of_elements) {
        buffer->head_pointer -= buffer->number_of_elements;
    }

    return 0;
}

void *circular_buffer_peek(struct CircularBuffer *buffer) {
    if (buffer->is_empty) {
        printf("[CircularBuffer](peek): Cannot peek, no data available!\n");
        return NULL;
    }
    return buffer->data + buffer->tail_pointer * buffer->element_size;
}

void *circular_buffer_pop(struct CircularBuffer *buffer) {
    if (buffer->is_empty) {
        printf("[CircularBuffer](pop): Cannot pop, no data available!\n");
        return NULL;
    }

    //Get the data, peek does this for us
    void *return_data = circular_buffer_peek(buffer);

    //Move tail, reset if we reached end of buffer
    buffer->tail_pointer++;
    if (buffer->tail_pointer >= buffer->number_of_elements) {
        buffer->tail_pointer -= buffer->number_of_elements;
    }

    //Check if we have read all data
    if (buffer->tail_pointer == buffer->head_pointer) {
        buffer->is_empty = true;
    }

    return return_data;
}

size_t circular_buffer_size(struct CircularBuffer *buffer) {
    if (buffer->tail_pointer > buffer->head_pointer) {
        return buffer->head_pointer + (buffer->number_of_elements - buffer->tail_pointer);

    } else if (!buffer->is_empty && buffer->tail_pointer == buffer->head_pointer) {
        return buffer->number_of_elements; //Buffer is completely full

    } else {
        return buffer->head_pointer - buffer->tail_pointer;
    }
}

size_t circular_buffer_available(struct CircularBuffer *buffer) {
    return buffer->number_of_elements - circular_buffer_size(buffer);
}

void circular_buffer_reset(struct CircularBuffer *buffer) {
    buffer->is_empty = true;
    buffer->head_pointer = 0;
    buffer->tail_pointer = 0;
}

void circular_buffer_log(struct CircularBuffer *buffer) {
    printf("[CircularBuffer](log): Head position: %d Tail position: %d Size: %d Available: %d Is empty: %d\n",
           (int) buffer->head_pointer, (int) buffer->tail_pointer, (int) circular_buffer_size(buffer),
           (int) circular_buffer_available(buffer), buffer->is_empty);
}
