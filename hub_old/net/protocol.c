#include "protocol.h"

#include <netinet/in.h>
#include <stdio.h>
#include <string.h>

uint16_t protocol_get_message_length(void *buffer) {
    uint16_t *message_length_network = buffer;
    return ntohs(*message_length_network);
}

int protocol_check_buffer(void *buffer, size_t bytes_in_buffer) {
    if (bytes_in_buffer < SIZEOF_U_INT16) return -1;

    return protocol_get_message_length(buffer) <= (bytes_in_buffer - SIZEOF_U_INT16);
}

int protocol_append_message_to_buffer(void *buffer, size_t available, struct ProtocolMessage *message) {
    if (!message) {
        perror("[Protocol](append_message_to_buffer): Cannot append null message");
        return -1;
    }

    if (available < message->data_length + SIZEOF_U_INT16) {
        perror("[Protocol](append_message_to_buffer): Not enough space in buffer for message");
        return -1;
    }

    if (message->data_length > 0 && !message->data) {
        perror("[Protocol](append_message_to_buffer): Data suggests data length of zero, but it is larger");
        return -1;
    }

    //Append message length
    memcpy(buffer, &message->data_length, SIZEOF_U_INT16);

    //Append actual message
    if (message->data != NULL) memcpy(buffer + SIZEOF_U_INT16, message->data, message->data_length);

    return 0; //Success
}


