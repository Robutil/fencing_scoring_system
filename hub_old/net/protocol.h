#ifndef C_PROTOCOL_H
#define C_PROTOCOL_H

#include <stdint.h>
#include <glob.h>

#define SIZEOF_U_INT16 2

struct ProtocolMessage {
    uint16_t data_length;
    void *data;
};

uint16_t protocol_get_message_length(void *buffer);

int protocol_check_buffer(void *buffer, size_t bytes_in_buffer);

int protocol_append_message_to_buffer(void *buffer, size_t available, struct ProtocolMessage *message);

#endif //C_PROTOCOL_H
