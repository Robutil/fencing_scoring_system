#ifndef C_SERVER_H
#define C_SERVER_H

#include <glob.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>

#include "../buffers/linear_buffer.h"
#include "../buffers/binary_tree.h"

struct ServerData {
    int max_pending_clients;
    int socket;
    bool active;
    fd_set *fd_read;
    fd_set *fd_active;

    struct BinaryTree *clients;

    void (*handler)(struct ServerData *client_data);
};

struct ClientData {
    int socket;
    struct LinearBuffer *buffer;
};

struct ServerData *server_create(int port);

int server_start(struct ServerData *server_data);

int server_stop(struct ServerData *server_data);

int server_destroy(struct ServerData *server_data);


#endif //C_SERVER_H
