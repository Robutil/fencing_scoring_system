#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>

#include "server.h"

#define SOCKET_ERROR (-1)
#define DEFAULT_MAX_PENDING_CLIENTS 1024


void
_server_check_fds(fd_set *fd_read, struct ServerData *server_data, void (*handler)(struct ClientData *client_data)) {
    for (int i = 0; i < FD_SETSIZE; ++i) {
        if (!FD_ISSET(i, fd_read)) continue;

        if (i == server_data->socket) {
            // New client
        } else {
            // New message
        }
    }
}

struct ServerData *server_create(int port) {
    if (port <= 0) {
        fprintf(stderr, "[Server](create): Cannot use port %d\n", port);
        return NULL;
    }

    struct ServerData *server_data = malloc(sizeof(struct ServerData));
    if (!server_data) {
        perror("[Server](create): Cannot allocate memory for container");
        return NULL;
    }

    //Get info
    struct addrinfo hints, *server_info = NULL;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    char port_buffer[10];
    sprintf(port_buffer, "%d", port);
    if (getaddrinfo(NULL, port_buffer, &hints, &server_info) != 0) {
        perror("[Server](create): Address info failed.");
        goto abort;
    }

    //Create socket
    server_data->socket = socket(hints.ai_family, hints.ai_socktype, hints.ai_flags);
    if (server_data->socket == SOCKET_ERROR) {
        perror("[Server](create): Cannot create socket");
        goto abort;
    }

    //Make socket reusable
    int enable = 1;
    if (setsockopt(server_data->socket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == SOCKET_ERROR) {
        perror("[Server](create): Cannot set socket to reusable");
        goto abort;
    }

    //Bind
    if (bind(server_data->socket, server_info->ai_addr, server_info->ai_addrlen) == SOCKET_ERROR) {
        perror("[Server](create): Cannot bind");
        goto abort;
    }

    //Success
    server_data->max_pending_clients = DEFAULT_MAX_PENDING_CLIENTS;
    server_data->active = false;
    server_data->clients = NULL;
    return server_data;

    abort:
    free(server_data);
    free(server_info);
    return NULL;
}

int server_start(struct ServerData *server_data) {
    if (!server_data) {
        fprintf(stderr, "[Server](serve): Cannot serve null\n");
        return -1;
    }

    //Start listening for incoming connections
    if (listen(server_data->socket, server_data->max_pending_clients) == SOCKET_ERROR) {
        perror("[Server](serve): Cannot listen");
        return -1;
    }
    server_data->active = true;

    //Set synchronous I/O
    server_data->fd_active = malloc(sizeof(fd_set));
    server_data->fd_read = malloc(sizeof(fd_set));
    FD_ZERO(server_data->fd_active);
    FD_SET(server_data->socket, server_data->fd_read);

    return 0;
}

int server_stop(struct ServerData *server_data) {
    return close(server_data->socket);
}

int server_destroy(struct ServerData *server_data) {
    free(server_data->fd_active);
    server_data->fd_active = NULL;
    free(server_data->fd_read);
    server_data->fd_read = NULL;
    binary_tree_destroy(server_data->clients);
}

void server_serve(struct ServerData *server_data, void (*handler)(struct ClientData *client_data)) {
    while (server_data->active) {
        fd_set *fd_read = server_data->fd_active;

        if (pselect(FD_SETSIZE, fd_read, NULL, NULL, NULL, NULL) == SOCKET_ERROR) {
            perror("[Server](serve): Select failed");
            server_data->active = false;
            return;
        }

        _server_check_fds(fd_read, server_data, handler);
    }
}