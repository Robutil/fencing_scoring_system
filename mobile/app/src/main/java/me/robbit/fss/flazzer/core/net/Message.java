package me.robbit.fss.flazzer.core.net;

public class Message {
    private byte id;
    private byte[] data;

    public Message(byte id) {
        this.id = id;
        data = new byte[0];
    }

    public Message(byte id, byte[] data) {
        this.id = id;
        this.data = data;
        if (this.data == null) {
            this.data = new byte[0];
        }
    }

    public byte getId() {
        return id;
    }

    public byte[] getData() {
        return data;
    }
}
