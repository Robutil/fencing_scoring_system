package me.robbit.fss.flazzer.core.packets;

import android.util.Log;

import java.io.IOException;

import me.robbit.fss.flazzer.MainActivity;
import me.robbit.fss.flazzer.core.net.Message;
import me.robbit.fss.flazzer.core.net.MessageCallback;
import me.robbit.fss.flazzer.core.net.Protocol;

public class PacketHandler implements MessageCallback {

    public static final byte PACKET_PING = 1;
    public static final byte PACKET_REGISTER_OBS = 4;
    public static final byte PACKET_REGISTER_OBS_ACK = 5;
    public static final byte PACKET_LIGHT_CHANGE = 8;
    public static final byte PACKET_GAMELIST_REQ = 9;
    public static final byte PACKET_GAMELIST_REP = 10;
    public static final byte PACKET_OBS_WATCH_GAME_REQ = 11;
    public static final byte PACKET_OBS_WATCH_GAME_REP = 12;

    private byte current_game_id = -1;

    private LightCallback lightCallback;

    public PacketHandler(LightCallback lightCallback) {
        this.lightCallback = lightCallback;
    }

    @Override
    public void onMessage(Protocol protocol, Message message) {
        switch (message.getId()) {
            case PACKET_LIGHT_CHANGE:
                handleLightChange(message);
                break;

            case PACKET_GAMELIST_REP:
                handleGameList(protocol, message);
                break;

            default:
                Log.i(MainActivity.TAG, "[PacketHandler](onMessage): Ignoring packet id " + message.getId());
        }
    }

    private void handleLightChange(Message message) {
        if (message.getData() == null || message.getData().length < 1) {
            Log.e(MainActivity.TAG, "[PacketHandler](handleLightChange): Empty payload, cannot parse");
        }
        byte lightId = (byte) (message.getData()[0] & 0x03);
        byte lightState = (byte) ((message.getData()[0] & 0xff) >> 7);
        switch (lightId) {
            case 0:
                lightCallback.onPlayerOnePrimary(lightState);
                break;
            case 1:
                lightCallback.onPlayerOneSecondary(lightState);
                break;
            case 2:
                lightCallback.onPlayerTwoPrimary(lightState);
                break;
            case 3:
                lightCallback.onPlayerTwoSecondary(lightState);
                break;
            default:
                Log.e(MainActivity.TAG, "[PacketHandler](handleLightChange): Unknown lightId: " + lightId);
        }
    }

    private void handleGameList(Protocol protocol, Message message) {
        if (message.getData().length == 0) {
            Log.i(MainActivity.TAG, "[PacketHandler](handleGameList): No games");
        }
        byte[] target_game = new byte[1];
        target_game[0] = message.getData()[0];
        Log.i(MainActivity.TAG, "[PacketHandler](handleGameList): Attempting register for game: " + Byte.toString(target_game[0]));
        Message observer_watch_request = new Message(PACKET_OBS_WATCH_GAME_REQ, target_game);
        try {
            protocol.sendMessage(observer_watch_request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
