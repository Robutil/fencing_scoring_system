package me.robbit.fss.flazzer.core.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import me.robbit.fss.flazzer.core.packets.PacketHandler;

public class ConnectionHandler implements Runnable {
    private String address = "192.168.1.101";
    private int port = 58080;

    /* TODO: Move this stuff to Protocol */
    private final int BUFFER_SIZE = 512;
    private int read_offset = 0, write_offset = 0;
    private byte[] buffer = new byte[BUFFER_SIZE];

    private MessageCallback messageCallback;
    private Protocol protocol;

    public ConnectionHandler(MessageCallback messageCallback) {
        this.messageCallback = messageCallback;
    }

    @Override
    public void run() {
        try {
            Socket server = new Socket(address, port);
            protocol = new Protocol(server);
            InputStream inputStream = server.getInputStream();
            OutputStream outputStream = server.getOutputStream();

            /* Register as observer */
//            byte[] packet_register = {0, 1, 4};
            Message packet_register = new Message(PacketHandler.PACKET_REGISTER_OBS);
            protocol.sendMessage(packet_register);

            while (server.isConnected()) {
                int bytesRead = inputStream.read(buffer, write_offset, BUFFER_SIZE - write_offset);
                write_offset += bytesRead;

                if (bytesRead <= 0) {
                    /* Something went wrong */
                    server.close();
                    return;
                }

                //noinspection StatementWithEmptyBody
                while (checkForMessage()) ;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean checkForMessage() {
        if (read_offset + 2 > write_offset) return false;

        /* Payload length is 16 bit */
        int packet_size = ((buffer[read_offset] & 0xFF) << 8 | (buffer[read_offset + 1] & 0xFF));

        /* Check if we have enough bytes in buffer for a full packet */
        if (packet_size + 2 <= write_offset - read_offset) {
            /* Full packet TODO: handle zero length packets */

            /* TODO: remove magic numbers -> maybe use java wrapper class instead */
            /* Parse packet into tidy Message */
            Message message;
            byte packet_id = buffer[read_offset + 2];
            if (packet_size > 1) {
                byte[] msg_buffer = new byte[packet_size - 1];
                System.arraycopy(buffer, read_offset + 3, msg_buffer, 0, packet_size - 1);
                message = new Message(packet_id, msg_buffer);
            } else message = new Message(packet_id);

            read_offset += (2 + packet_size);

            /* Notify callback */
            messageCallback.onMessage(protocol, message);
            return true;
        }
        return false;
    }
}
