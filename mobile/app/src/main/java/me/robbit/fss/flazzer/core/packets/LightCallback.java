package me.robbit.fss.flazzer.core.packets;

public interface LightCallback {
    void onPlayerOnePrimary(int state);

    void onPlayerOneSecondary(int state);

    void onPlayerTwoPrimary(int state);

    void onPlayerTwoSecondary(int state);
}
