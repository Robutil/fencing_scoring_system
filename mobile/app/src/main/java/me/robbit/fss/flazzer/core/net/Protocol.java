package me.robbit.fss.flazzer.core.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Protocol {
    private Socket socket;
    private OutputStream outputStream;
    private InputStream inputStream;

    public Protocol(Socket socket) throws IOException {
        this.socket = socket;
        outputStream = socket.getOutputStream();
        inputStream = socket.getInputStream();
    }

    public void sendMessage(Message msg) throws IOException {
        short payload_length = (short) ((msg.getData().length + 1) & 0xffff);
        byte[] buffer = new byte[3];
        buffer[0] = (byte) (payload_length & 0xff00);
        buffer[1] = (byte) (payload_length & 0x00ff);
        buffer[2] = msg.getId();

        outputStream.write(buffer);
        if (msg.getData() != null) outputStream.write(msg.getData());
    }
}
