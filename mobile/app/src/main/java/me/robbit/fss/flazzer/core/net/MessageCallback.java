package me.robbit.fss.flazzer.core.net;

public interface MessageCallback {
    void onMessage(Protocol protocol, Message message);
}
