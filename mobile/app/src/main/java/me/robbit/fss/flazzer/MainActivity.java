package me.robbit.fss.flazzer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import me.robbit.fss.flazzer.core.net.ConnectionHandler;
import me.robbit.fss.flazzer.core.packets.LightCallback;
import me.robbit.fss.flazzer.core.packets.PacketHandler;
import me.robbit.fss.flazzer.core.sound.SoundService;

public class MainActivity extends AppCompatActivity implements LightCallback {

    public static final String TAG = "FLAZZER";

    private final int LIGHT_OFF = 0;
    private int p11 = 0, p12 = 0, p21 = 0, p22 = 0;
    private Button playerOnePrimary;
    private Button playerTwoPrimary;
    private Button playerOneSecondary;
    private Button playerTwoSecondary;

    SoundService soundService = new SoundService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initLights();

        PacketHandler packetHandler = new PacketHandler(this);
        ConnectionHandler connectionHandler = new ConnectionHandler(packetHandler);
        new Thread(connectionHandler).start();
    }

    void initLights() {
        playerOnePrimary = findViewById(R.id.activityMainPlayerOnePrimary);
        playerTwoPrimary = findViewById(R.id.activityMainPlayerTwoPrimary);
        playerOneSecondary = findViewById(R.id.activityMainPlayerOneSecondary);
        playerTwoSecondary = findViewById(R.id.activityMainPlayerTwoSecondary);

        playerOnePrimary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p11 = Math.abs(p11 - 1);
                onPlayerOnePrimary(p11);
            }
        });
        playerOneSecondary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p12 = Math.abs(p12 - 1);
                onPlayerOneSecondary(p12);
            }
        });
        playerTwoPrimary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p21 = Math.abs(p21 - 1);
                onPlayerTwoPrimary(p21);
            }
        });
        playerTwoSecondary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p22 = Math.abs(p22 - 1);
                onPlayerTwoSecondary(p22);
            }
        });
    }

    private void parseColor(Button b, int code) {
        int parsedCode = getResources().getColor(code, null);
        b.setBackgroundColor(parsedCode);

        if (code != android.R.color.darker_gray) {
            soundService.run();
        }
    }

    @Override
    public void onPlayerOnePrimary(int state) {
        int color = state == LIGHT_OFF ? android.R.color.darker_gray : android.R.color.holo_green_light;
        parseColor(playerOnePrimary, color);
        p11 = state;
    }

    @Override
    public void onPlayerOneSecondary(int state) {
        int color = state == LIGHT_OFF ? android.R.color.darker_gray : android.R.color.white;
        parseColor(playerOneSecondary, color);
        p12 = state;
    }

    @Override
    public void onPlayerTwoPrimary(int state) {
        int color = state == LIGHT_OFF ? android.R.color.darker_gray : android.R.color.holo_red_light;
        parseColor(playerTwoPrimary, color);
        p21 = state;
    }

    @Override
    public void onPlayerTwoSecondary(int state) {
        int color = state == LIGHT_OFF ? android.R.color.darker_gray : android.R.color.white;
        parseColor(playerTwoSecondary, color);
        p22 = state;
    }
}
