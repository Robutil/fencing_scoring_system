# Fencing scoring system

The aim of this project is to create an affordable wireless fencing
scoring system.

## Table of contents

* [How it works](#how_it_works)
* [Project structure](#project_structure)
* [Limitations](#limitations)
* [Literature](#literature)
* [License](#license)

## How it works <a name="how_it_works"></a>

For the ease-of-use of the fencers this project will not promote 
technologies that require modifications of a fencer's standard equipment,
such as: the weapon, wiring, and the metal vest.

Each fencer will be equipped with a wireless unit which will interface with
wiring that is the current norm. Due to the fact that the units are wireless
there is no common ground between the fencers, which means that there is no
return path for a flow of current. To solve this issue we generate alternating
current (AC) with a specific frequency. This signal can be measured by the
other wireless module via the metal vest. In case of epee, there is no metal
vest and any hit will be seen as valid.

## Project structure <a name="project_structure"></a>

The system is composed of two required components and one optional component.
The required components are the wireless modules that the fencers carry and
the hub that handles lights to show (valid) hits. The wireless modules will
henceforth be called clients, the hub will simply be called hub. As an optional
component a mobile phone can be connected with the hub, which can allow for
a variety of new features in the future (e.g. keep track of points). For
now the mobile phone can mimic the hub lights.

## Limitations <a name="limitations"></a>

In an effort to simplify our main focus lies on the foil (fleur) weapon,  
we do intent for the solution to work for all three weapons. The main
consequence of this decision is that, for now, we do not take a metal piste
into account.

## Literature <a name="literature"></a>

* [The Design of a Wireless Scoring System for Epee Fencing ](http://www.worldacademicunion.com/journal/SSCI/SSCIvol06no04paper06.pdf)
* [Design of a Wireless Scoring System for Fencing Using RFID
 Technology](https://pdfs.semanticscholar.org/8465/df7305302677caeccee0420e387445b73227.pdf?_ga=2.64232429.311586157.1539622673-342768410.1539622673)

## License <a name="license"></a>

This project is licensed under the GNU GPLv3 License, see the 
[LICENSE.md](LICENSE.md) file for details.
