import numpy as np
import matplotlib.pyplot as plt

samples = np.load('dummy.npy')
test_data = samples[:len(samples) // 2]
calibrate_data = samples[len(samples) // 2:]


def calibrate(m_samples):
    meds = []
    for i in range(0, len(m_samples), 10):
        if i + 10 > len(m_samples):
            break
        meds.append(sorted(m_samples[i:i + 10])[5])
    return sorted(meds)[len(meds) // 2]


def count_peaks(m_samples, threshold):
    prev_value = 0
    peaks = 0
    positions = []
    for i, sample in enumerate(m_samples):
        sample = sample[0]
        if abs(prev_value - sample) > threshold:
            peaks += 1
            positions.append((i, sample))
        prev_value = sample
    return peaks, positions


calibration = calibrate(m_samples=calibrate_data)
peaks, debug_graph = count_peaks(test_data, calibration)

print('Found ' + str(peaks) + ' peaks')
plt.plot(*zip(*debug_graph), 'ro', label='peaks')
plt.plot(test_data, label='signal')
plt.legend()
plt.show()