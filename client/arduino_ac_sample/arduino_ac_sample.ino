const int BUFFER_SIZE = 512; /* Must be even and within memory limit */
int buffer[BUFFER_SIZE];
int max_count, counter = 0;

void setup(){
  Serial.begin(9600);
  max_count = BUFFER_SIZE / 2; /* Each sample takes up two bytes */
}

void loop(){
  buffer[counter++] = analogRead(A6);
  if(counter == max_count){
    Serial.write((uint8_t*) buffer, BUFFER_SIZE);
    counter = 0; /* Reset */
  }
}
