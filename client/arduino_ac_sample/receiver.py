import struct
import serial
import numpy as np
import matplotlib.pyplot as plt

# Set up serial connection
ser = serial.Serial('/dev/ttyUSB0', 9600)  # '/dev/tty.usbserial', 9600

# Read bytes into buffer until disconnect
buffer = []
payload_length = 512
counter = 0
first_run = True
force_sync = False

while ser.isOpen() and counter < 10:
    raw_data = ser.read(payload_length)
    if raw_data is None:
        break

    if first_run:
        first_run = False
        iterator = struct.iter_unpack('<H', raw_data)
        for val in iterator:
            if val[0] > 1024:
                force_sync = True
                break

    if force_sync:
        raw_data = raw_data[1:-1]

    iterator = struct.iter_unpack('<H', raw_data)
    for val in iterator:
        buffer.append(val)

    counter += 1
    print(counter)

ser.close()
# buffer = buffer[len(buffer) // 2:]
plt.plot(buffer)
plt.show()

np.save(file='dummy', arr=np.asarray(buffer))
