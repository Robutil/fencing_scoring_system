void setup() {
  Serial.begin(9600);
}

int lastValue = 0;
unsigned long start_ms = 0;
int counter = -1;

void loop() {
  int sensorValue = analogRead(A0);
  if (abs(lastValue - sensorValue) >= 0){
    unsigned long cur_ms = millis();
    
    if (counter == -1){
      start_ms = cur_ms;
      counter = 0;
    } else counter++;
    
    if(cur_ms - start_ms > 1000){
      Serial.print(counter);
      Serial.println(" pulses");
      counter = -1;
    }
    
//    Serial.print(abs(lastValue - sensorValue));
//    Serial.print(" ");
//    Serial.println(sensorValue);
  }
  lastValue = sensorValue;
}
