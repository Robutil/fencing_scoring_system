/* 1 - GREEN - Receiver */
int lastValue = 0;
unsigned long start_ms = 0;
int counter = 0;

void setup() {
  //Serial.begin(9600);
  start_ms = millis();
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  int sensorValue = analogRead(A0);
  if (abs(lastValue - sensorValue) >= 50) {
    counter++;
  }
  lastValue = sensorValue;


  unsigned long cur_ms = millis();
  if (cur_ms - start_ms > 1000) {
    //Serial.print(counter);
    //Serial.println(" pulses");
    int status = counter < 200 ? HIGH : LOW;
    digitalWrite(LED_BUILTIN, status);
    counter = 0;
    start_ms = cur_ms;
  }
}

/* 2 - RED - Transmitter */

//int tp = D7; /* Transmitter pin */
//void setup() {
//  pinMode(tp, OUTPUT);
//}
//
//void loop() {
//  digitalWrite(tp, HIGH);
//  delay(1);
//  digitalWrite(tp, LOW);
//  delay(1);
//}
