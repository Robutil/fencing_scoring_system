#include <ESP8266WiFi.h>
#include <SoftwareSerial.h>

#include <stdint.h>

struct Message {
  void* data;
  int length;  
};

const char* ssid = "doot";
const char* password = "RobbinDraadloos1";
const char* host = "192.168.1.101";
const int port = 58080;

const bool DEBUG = true;
WiFiClient client;

void setup() {
  Serial.begin(9600);
  delay(10); //Settle

  /* Use BUILTIN_LED to display some status when there is no serial */
  pinMode(BUILTIN_LED, OUTPUT);
  digitalWrite(BUILTIN_LED, LOW);

  /* Connect to WIFI network */
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    if(DEBUG) Serial.write("(setup): Trying to connect to wifi network...\n");
    delay(500);
  }
}

void prot_send(void* data, uint16_t data_length){
  /* Endianess, send payload length (2 bytes) */
  uint8_t *prot_helper = (uint8_t*) &data_length;
  client.write(*(prot_helper + 1));
  client.write(*prot_helper);

  /* Send data */
  client.write((const uint8_t *) data, data_length);
}

uint8_t get_byte() {
  while (1) {
    if (client.available()) return client.read();
    else delay(1);
  }
}

void register_as_client(){
  char buffer[3] = {2, 67, 0};
  prot_send(buffer, 3);
}

void destroy_message(struct Message *msg){
  if(msg == NULL) return;
  free(msg->data);
  free(msg);
}

struct Message *read_message(){
  /* Read payload length (2 bytes), reverse endianess */
  uint16_t prot_payload_length = 0;
  uint8_t *prot_helper = (uint8_t*) &prot_payload_length;
  *(prot_helper + 1) = get_byte();
  *prot_helper = get_byte();
  if(DEBUG) Serial.print("(read_message): Received payload length: ");
  if(DEBUG) Serial.println(prot_payload_length);

  /* Even though this message is possible within the protocol
   * it is more likely an endianess error. Abort. */
  if(prot_payload_length > 255) return NULL;

  /* Read payload */
  char* buffer = (char* ) malloc(prot_payload_length);
  if(DEBUG) Serial.println("(read_message): Allocated message buffer");
  if(buffer == NULL) return NULL;
  for(int i = 0; i < prot_payload_length; i++){
    buffer[i] = get_byte();
  }

  /* Put message into data structure */
  struct Message *msg = (struct Message*) malloc(sizeof(struct Message*));
  if(DEBUG) Serial.println("(read_message): Allocated msg struct");
  if(msg == NULL){
    free(buffer);
    free(msg);
  }
  msg->data = buffer;
  msg->length = prot_payload_length;
  return msg;
}

void loop() {
  /* Connect to server */
  if(DEBUG) Serial.write("(loop): Trying to connect to server...\n");
  if (!client.connect(host, port)) {
    delay(1000);
    return; //to loop
  }
  if(DEBUG) Serial.write("(loop): Connected!\n");

  /* Send register as client request packet */
  register_as_client();

  /* Read request response */
  struct Message *msg = read_message();
  if(DEBUG) Serial.println("(loop): Got a new message");
  if(msg && msg->length >= 2){
    if(DEBUG) Serial.println("(loop): Message can be read");
    uint8_t *packet_id = (uint8_t *) msg->data;
    uint8_t *res = (uint8_t *) (msg->data + 1);
    
    if(*packet_id == 3 && *res == 0){
      if(DEBUG) Serial.println("(loop): Registration success!");
      digitalWrite(BUILTIN_LED, HIGH); /* Visual notifyer that we connected */
      delay(2000); /* Show visual for a bit, then the LED can be re-used */
    } else if(DEBUG) Serial.println("(loop): Unknown packet or registration failed");
    
    destroy_message(msg);  
  } else if(DEBUG) Serial.println("(loop): Empty or unexpected packet");

  /* Reset cleanly */
  if(DEBUG) Serial.println("(loop): Stopping connection with server");
  client.stop();
  delay(5000);
}
