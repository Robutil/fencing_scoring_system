#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdbool.h>

#include "server.h"

#define NO_FLAGS 0
#define SOCKET_ERROR (-1)
#define DEFAULT_MAX_PENDING_CLIENTS 8

struct ServerData {
    int socket;
    bool active;

    void (*on_connect)(int, struct sockaddr_in *);

    int (*on_message)(int);

    void (*on_close)(int);

    void (*on_tick)(void);
};

void server_set_on_connect(struct ServerData *server_data, void (*on_connect)(int, struct sockaddr_in *)) {
    server_data->on_connect = on_connect;
}

void server_set_on_message(struct ServerData *server_data, int (*on_message)(int)) {
    server_data->on_message = on_message;
}

void server_set_on_close(struct ServerData *server_data, void (*on_close)(int)) {
    server_data->on_close = on_close;
}

void server_set_on_tick(struct ServerData *server_data, void (*on_tick)(void)) {
    server_data->on_tick = on_tick;
}

struct ServerData *server_create(uint16_t port) {
    struct ServerData *server_data = calloc(sizeof(struct ServerData), 1);
    if (!server_data) {
        perror("[Server](create): Cannot allocate memory for container");
        return NULL;
    }

    //Create socket
    server_data->socket = socket(AF_INET, SOCK_STREAM, NO_FLAGS);
    if (server_data->socket == SOCKET_ERROR) {
        perror("[Server](create): Cannot create socket");
        goto abort;
    }

    //Make socket reusable
    int enable = 1;
    if (setsockopt(server_data->socket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == SOCKET_ERROR) {
        perror("[Server](create): Cannot set socket to reusable");
        goto abort;
    }

    //Bind
    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(port);
    if (bind(server_data->socket, (struct sockaddr *) &server, sizeof(server)) == SOCKET_ERROR) {
        perror("[Server](create): Cannot bind");
        close(server_data->socket);
        goto abort;
    }

    //Success
    server_data->active = false;
    return server_data;

    abort:
    free(server_data);
    return NULL;
}

int _handle_new_client(struct ServerData *server_data, fd_set *active) {
    printf("[Server](_handle_new_client): New client\n");

    struct sockaddr name;
    socklen_t size = sizeof(struct sockaddr);
    int client_socket = accept(server_data->socket, &name, &size);
    if (client_socket < 0) return SOCKET_ERROR;

    FD_SET(client_socket, active);
    if (server_data->on_connect != NULL) {
        server_data->on_connect(client_socket, (struct sockaddr_in *) &name);
    }
    return client_socket;
}

int _handle_new_message(struct ServerData *server_data, fd_set *active, const int *sock) {
    printf("[Server](_handle_new_message): Incoming message\n");

    if(server_data->on_message(*sock) == -1){
        /* Disconnect client */ 
        if (server_data->on_close != NULL) {
            server_data->on_close(*sock);
        }
        close(*sock);
        FD_CLR(*sock, active);
        return SOCKET_ERROR;
    }
    
    return 0;
}

void server_serve(struct ServerData *server_data, int timeout_ms) {
    fd_set active_fd_set, read_fd_set;
    FD_ZERO(&active_fd_set);
    FD_SET(server_data->socket, &active_fd_set);

    struct timespec *tv = NULL;
    if (timeout_ms) {
        tv = malloc(sizeof(struct timespec));
        tv->tv_sec = timeout_ms / 1000;
        tv->tv_nsec = (timeout_ms % 1000) * 1000000;
    }

    int select_status;
    while (server_data->active) {
        read_fd_set = active_fd_set;

        select_status = pselect(FD_SETSIZE, &read_fd_set, NULL, NULL, tv, NULL);
        if (select_status == SOCKET_ERROR) {
            perror("[Server](serve): Select failed");
            server_data->active = false;
            free(tv);
            return;
        } else if (select_status) {
            for (int i = 0; i < FD_SETSIZE; ++i) {
                if (!FD_ISSET(i, &read_fd_set)) continue;

                if (i == server_data->socket) {
                    _handle_new_client(server_data, &active_fd_set);
                    /* TODO: Care about connections */

                } else {
                    _handle_new_message(server_data, &active_fd_set, &i);
                }
            }
        }
        if (server_data->on_tick) { server_data->on_tick(); }
    }
    free(tv);
}

int server_start(struct ServerData *server_data, int timeout_ms) {
    if (!server_data) {
        fprintf(stderr, "[Server](serve): Cannot serve null\n");
        return -1;
    }

    //Start listening for incoming connections
    if (listen(server_data->socket, DEFAULT_MAX_PENDING_CLIENTS) == SOCKET_ERROR) {
        perror("[Server](serve): Cannot listen");
        return -1;
    }
    server_data->active = true;

    server_serve(server_data, timeout_ms);
    return 0;
}

int server_stop(struct ServerData *server_data) {
    return close(server_data->socket);
}

int server_destroy(struct ServerData *server_data) {
    free(server_data);
    return 0;
}