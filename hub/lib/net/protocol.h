#ifndef HUB_V2_PROTOCOL_H
#define HUB_V2_PROTOCOL_H

#include "message.h"
#include "../buffer/linear_buffer.h"

struct Message *protocol_check_buffer(struct LinearBuffer *buffer);

int protocol_send_message(int fd, struct Message *msg);

void protocol_destroy_message(struct Message *msg);

#endif //HUB_V2_PROTOCOL_H
