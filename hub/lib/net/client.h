#ifndef HUB_V2_CLIENT_H
#define HUB_V2_CLIENT_H

#include <stdint.h>

int connect_to_server(char *address, uint16_t port);

int close_server_connection(int conn_fd);

#endif //HUB_V2_CLIENT_H
