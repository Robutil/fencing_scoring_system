#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdio.h>

#include "protocol.h"

struct Message *protocol_check_buffer(struct LinearBuffer *buffer) {
    size_t available = linear_buffer_size(buffer);

    if (available < 2) return NULL;

    void *data = linear_buffer_peek(buffer);
    uint16_t *convert = data;
    uint16_t payload_length = ntohs(*convert);

    if ((available - 2) < payload_length) return NULL;

    struct Message *msg = malloc(sizeof(struct Message));
    buffer->tail_pointer += 2;
    msg->data = linear_buffer_pop_bytes(buffer, payload_length);
    msg->size = payload_length;
    return msg;
}

int protocol_send_message(int fd, struct Message *msg) {
    uint16_t payload_length = htons((uint16_t) msg->size); /* TODO: handle larger packets */

    ssize_t bytes_written = write(fd, &payload_length, 2);
    if (bytes_written != 2) return -1;

    bytes_written = write(fd, msg->data,  msg->size);
    return bytes_written == msg->size ? 0 : -1;
}

void protocol_destroy_message(struct Message *msg) {
    free(msg->data);
    free(msg);
}
