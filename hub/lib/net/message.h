#ifndef HUB_V2_MESSAGE_H
#define HUB_V2_MESSAGE_H

#include <stddef.h>

struct Message {
    void *data;
    size_t size;
};

#endif //HUB_V2_MESSAGE_H
