#ifndef C_SERVER_H
#define C_SERVER_H

#include <stdint.h>
#include <netinet/in.h> /* sockaddr_in */

#include "message.h"

#define NO_TIMEOUT 0

struct ServerData;

struct ServerData *server_create(uint16_t port);

int server_start(struct ServerData *server_data, int timeout_ms);

int server_stop(struct ServerData *server_data);

int server_destroy(struct ServerData *server_data);

void server_set_on_connect(struct ServerData *server_data, void (*on_connect)(int fd, struct sockaddr_in *info));

void server_set_on_message(struct ServerData *server_data, int (*on_message)(int fd));

void server_set_on_close(struct ServerData *server_data, void (*on_close)(int fd));

void server_set_on_tick(struct ServerData *server_data, void (*on_tick)(void));

#endif //C_SERVER_H
