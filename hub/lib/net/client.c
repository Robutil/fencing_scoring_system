#include <arpa/inet.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>

#include "client.h"

int connect_to_server(char *address, uint16_t port) {
    int ret = 0;
    int conn_fd;
    struct sockaddr_in server_addr = {0};
    server_addr.sin_family = AF_INET;

    server_addr.sin_port = htons(port);
    ret = inet_pton(AF_INET, address, &server_addr.sin_addr);
    if (ret != 1) {
        if (ret == -1) {
            perror("inet_pton");
        }
        fprintf(stderr, "failed to convert address %s to binary net address\n", address);
        return -1;
    }

    fprintf(stdout, "[Client](connect_to_server): Connecting to %s on port %d\n", address, port);

    conn_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (conn_fd == -1) {
        perror("socket");
        return -1;
    }

    ret = connect(conn_fd, (struct sockaddr *) &server_addr, sizeof(server_addr));
    if (ret == -1) {
        perror("connect");
        return -1;
    }
    fprintf(stdout, "[Client](connect_to_server): Connection established\n");
    return conn_fd;
}

int close_server_connection(int conn_fd) {
    fprintf(stdout, "[Client](close_server_connection): Shutting down connection\n");
    int ret = shutdown(conn_fd, SHUT_RDWR);
    if (ret == -1) {
        perror("shutdown");
        return -1;
    }
    ret = close(conn_fd);
    if (ret == -1) {
        perror("close");
        return -1;
    }
    return ret;
}