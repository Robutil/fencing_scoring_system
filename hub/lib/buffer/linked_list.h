#ifndef C_LINKED_LIST_H
#define C_LINKED_LIST_H

/**
 * A linked list is a data type where each link has a pointer to another
 * linked list element. In this case the linked list is doubled; each
 * element contains a pointer to its parent and its child. Each linked
 * list element has a data field.
 * */
struct LinkedList;

struct LinkedListNode;

/**
 * Creates a linked list link. If this is the first link created this will
 * count as the root element for a linked list.
 *
 * @param data Data to append to the link, data is copied
 * @param data_length Size of data
 *
 * @return A newly allocated LinkedList element
 * */
struct LinkedList *linked_list_create();

/**
 * Destroys the given link as well as the connected links.
 *
 * @param link Link to destroy, also destroys neighbours recursively
 * */
void linked_list_destroy(struct LinkedList *list);

/**
 * Creates a new LinkedList element and adds it after the provided link,
 * thus becoming its child. If the provided link already has a child, it
 * will be transferred to the newly created link.
 *
 * @param data Data to be stored in the new LinkedList element
 * @param data_length Length of data
 * @param link To be parent linked list element
 * */
int linked_list_insert_after(struct LinkedList *list, struct LinkedListNode *target_node, void *data);

/**
 * Creates a new LinkedList element and adds it before the provided link,
 * thus becoming its parent. If the provided link already has a parent, it
 * will be transferred to the newly created link.
 *
 * @param data Data to be stored in the new LinkedList element
 * @param data_length Length of data
 * @param link To be child linked list element
 * */
int linked_list_insert_before(struct LinkedList *list, struct LinkedListNode *target_node, void *data);

/**
 * Creates a new LinkedList element and adds it as the last element in the list.
 *
 *  @param data Pointer to data to copy to the newly created link
 *  @param data_length Amount of bytes to copy from data
 *  @param link Any link
 *
 *  @return 0 of the data was added successfully, -1 otherwise
 * */
int linked_list_push_back(struct LinkedList *list, void *data);

/**
 * Gets the root element of a given link. In case of a circular linked list
 * this function will return NULL since there is no root element.
 *
 * @param link Any link
 *
 * @return Pointer to the root element of the list or NULL on failure
 * */
struct LinkedListNode *linked_list_get_root(struct LinkedList *list);

/**
 * Gets the last element of a given link. In case of a circular linked list
 * this function will return NULL since there is no last element.
 *
 * @param link Any link
 *
 * @return Pointer to the last element of the list or NULL on failure
 * */
struct LinkedListNode *linked_list_get_last(struct LinkedList *list);

/**
 * Connects the root and last elements in a list. This will make the list
 * circular, meaning that it will have no start nor end. This operation can
 * fail (it will return -1) if the provided list is already circular.
 *
 * @param link Any link, the root and last element shall be found using lookups
 *
 * @return 0 if the list was made circular, -1 otherwise.
 * */
int linked_list_make_circular(struct LinkedList *list);

/**
 * Removes a given link from a list. The rest of the list will remain
 * intact and parents and children shall be inherited. All data in the
 * link shall be freed.
 *
 * @param link Link to remove
 * */
void linked_list_remove(struct LinkedList *list, struct LinkedListNode *node);

int linked_list_push_front(struct LinkedList *list, void *data);

struct LinkedListNode *linked_list_next(struct LinkedListNode *node);

struct LinkedListNode *linked_list_prev(struct LinkedListNode *node);

void *linked_list_get_data(struct LinkedListNode *node);

unsigned long linked_list_size(struct LinkedList *list);

struct LinkedListNode *linked_list_find(struct LinkedList *list, void *data);

void linked_list_foreach(struct LinkedList *list, int (*func)(struct LinkedListNode *));


#endif //C_LINKED_LIST_H
