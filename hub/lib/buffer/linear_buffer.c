#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "linear_buffer.h"

struct LinearBuffer *linear_buffer_create(size_t size) {
    struct LinearBuffer *buffer = malloc(sizeof(struct LinearBuffer));

    if (!buffer) {
        perror("[LinearBuffer](create): Cannot allocate memory for LinearBuffer container");
        return NULL;
    }

    //Housekeeping variables
    buffer->size = size;
    buffer->head_pointer = 0;
    buffer->tail_pointer = 0;

    //The actual buffer
    buffer->data = malloc(size);

    if (!buffer->data) {
        perror("[LinearBuffer](create): Cannot allocate memory buffer");
        free(buffer);
        return NULL;
    }

    return buffer;
}

void linear_buffer_destroy(struct LinearBuffer *buffer) {
    free(buffer->data);
    free(buffer);
}

int linear_buffer_push(struct LinearBuffer *buffer, void *data, size_t data_length) {
    //Check if we have the space
    if (buffer->head_pointer + data_length > buffer->size) {

        //Check if we can get the space by cleaning the buffer
        if (buffer->head_pointer + data_length - buffer->tail_pointer > buffer->size) {
            printf("[LinearBuffer](push): Cannot push data, insufficient space\n");
            return -1;

        } else {
            //Clean the buffer, we have enough space then
            linear_buffer_clean(buffer);
        }
    }

    //Append data to buffer
    memcpy(buffer->data + buffer->head_pointer, data, data_length);
    buffer->head_pointer += data_length;

    return 0; //Success
}

void *linear_buffer_peek(struct LinearBuffer *buffer) {
    //if (linear_buffer_size(buffer) == 0) return NULL; /* TODO: Not sure if this is desired */

    return buffer->data + buffer->tail_pointer;
}

void *linear_buffer_pop(struct LinearBuffer *buffer) {
    size_t bytes_in_buffer = linear_buffer_size(buffer);

    return linear_buffer_pop_bytes(buffer, bytes_in_buffer);
}

void *linear_buffer_pop_bytes(struct LinearBuffer *buffer, size_t length) {
    if (length == 0) return NULL; //Empty buffer request

    //Check if we have enough bytes to give
    if (buffer->tail_pointer + length > buffer->head_pointer) {
        printf("[LinearBuffer](pop_bytes): Cannot peek that many bytes, expecting push first\n");
    }

    void *return_data = malloc(length);
    if (!return_data) {
        perror("[LinearBuffer](pop_bytes): Unable to allocate data buffer");
        return NULL;
    }

    //Get data and increments bytes read (tail_pointer)
    memcpy(return_data, buffer->data + buffer->tail_pointer, length);
    buffer->tail_pointer += length;

    //Check if we can clean buffer without memmove
    if (linear_buffer_size(buffer) == 0) {
        linear_buffer_reset(buffer);
    }

    return return_data;
}

size_t linear_buffer_size(struct LinearBuffer *buffer) {
    return buffer->head_pointer - buffer->tail_pointer;
}

size_t linear_buffer_available(struct LinearBuffer *buffer) {
    return buffer->size - linear_buffer_size(buffer);
}

void linear_buffer_clean(struct LinearBuffer *buffer) {
    if (buffer->tail_pointer == 0) return; //Nothing to do

    //If data is zero this function is equal to a reset
    if (linear_buffer_size(buffer) == 0) {
        linear_buffer_reset(buffer);
        return;
    }

    //Bring all data to front
    memmove(buffer->data, buffer->data + buffer->tail_pointer, linear_buffer_size(buffer));
    buffer->head_pointer -= buffer->tail_pointer;
    buffer->tail_pointer = 0;
}

ssize_t linear_buffer_read_fd(int fd, struct LinearBuffer *buffer) {
    ssize_t bytes_read = read(fd, buffer->data + buffer->head_pointer, linear_buffer_available(buffer));
    if (bytes_read <= 0) return bytes_read; /* File descriptor issues */

    buffer->head_pointer += bytes_read; /* Increment bytes written */
    if (buffer->head_pointer == buffer->size) linear_buffer_clean(buffer); /* Full buffer, try to clean up */
    return bytes_read;
}

void linear_buffer_reset(struct LinearBuffer *buffer) {
    buffer->tail_pointer = 0;
    buffer->head_pointer = 0;
}

void linear_buffer_log(struct LinearBuffer *buffer) {
    printf("[LinearBuffer](log): Head position: %d Tail position: %d Size: %d Available: %d\n",
           (int) buffer->head_pointer, (int) buffer->tail_pointer, (int) linear_buffer_size(buffer),
           (int) linear_buffer_available(buffer));
}

