#ifndef HUB_V2_LINEAR_HASH_H
#define HUB_V2_LINEAR_HASH_H

struct LinearHash;

struct LinearHash *linear_hash_create(size_t size);

int linear_hash_destroy(struct LinearHash *linear_hash);

int linear_hash_insert(struct LinearHash *linear_hash, int key, void *value);

void *linear_hash_find(struct LinearHash *linear_hash, int key);

void *linear_hash_pop(struct LinearHash *linear_hash, int key);

#endif //HUB_V2_LINEAR_HASH_H
