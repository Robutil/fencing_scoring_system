#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "binary_tree.h"

/**
 * Niche utility function that returns the pointer address from its parents that
 * points to the given node.
 *
 * @param me Address to search for
 *
 * @return Pointer address where the pointer value points to the given address
 * */
struct BinaryTree **_binary_tree_get_own_pointer_from_parent(struct BinaryTree *me) {
    if (!me || me->parent == NULL) return NULL;

    if (me->parent->upper == me) {
        return &me->parent->upper;
    }
    return &me->parent->lower;
}

/**
 * Niche utility function that replaces all data in a given node with contents
 * from another node. The node where the data was copied from is then removed.
 *
 * @param node Node address to keep, content shall be purged
 * @param data Node to purge, data will be copied to <b>node<b>
 * */
void _binary_tree_replace_node(struct BinaryTree *node, struct BinaryTree *data) {
    //Purge old data
    free(node->data);

    //Copy "new" content
    node->id = data->id;
    node->data = data->data;
    node->data_length = data->data_length;

    //Remove old node trace
    struct BinaryTree **m = _binary_tree_get_own_pointer_from_parent(data);
    *m = NULL; //Remove data address pointer from data's parent

    //Clean up old node
    free(data);
}

struct BinaryTree *binary_tree_create(int id) {
    struct BinaryTree *new_root = calloc(sizeof(struct BinaryTree), 1);

    if (!new_root) {
        perror("[BinaryTree](create): Cannot allocate memory for container");
        return NULL;
    }

    new_root->id = id;
    return new_root;
}

void binary_tree_destroy(struct BinaryTree *root) {
    if (!root) return; //Nothing to do

    if (root->lower) binary_tree_destroy(root->lower);
    if (root->upper) binary_tree_destroy(root->upper);

    //Manual free's instead of removes to increase performance
    free(root->data);
    free(root);
}

void binary_tree_remove(struct BinaryTree *root, int id) {
    //Find link to remove
    struct BinaryTree *link_to_remove = binary_tree_get(root, id);
    if (!link_to_remove) return;

    //If we have no children, just remove parent reference
    if (!link_to_remove->lower && !link_to_remove->upper && link_to_remove->parent) {
        struct BinaryTree **m = _binary_tree_get_own_pointer_from_parent(link_to_remove);
        *m = NULL;

    } else if (link_to_remove->lower && !link_to_remove->upper) {
        //We only have one lower child, give it to parent
        if (link_to_remove->parent) {
            struct BinaryTree **m = _binary_tree_get_own_pointer_from_parent(link_to_remove);
            *m = link_to_remove->lower;
            link_to_remove->lower->parent = link_to_remove->parent;

        } else {
            //We are root, make our lower child the new root
            _binary_tree_replace_node(link_to_remove, link_to_remove->lower);
        }

    } else if (link_to_remove->upper && !link_to_remove->lower) {
        //We only have one upper child, give it to parent
        if (link_to_remove->parent) {
            struct BinaryTree **m = _binary_tree_get_own_pointer_from_parent(link_to_remove);
            *m = link_to_remove->upper;
            link_to_remove->upper->parent = link_to_remove->parent;

        } else {
            //We are root, make our upper child the new root
            _binary_tree_replace_node(link_to_remove, link_to_remove->upper);
        }

    } else if (link_to_remove->lower && link_to_remove->upper) {
        //We have two children, replace us with the smallest node among our upper children
        struct BinaryTree *minimum = binary_tree_find_minimum(link_to_remove->upper);

        _binary_tree_replace_node(link_to_remove, minimum);
        return;
    }

    free(link_to_remove->data);
    free(link_to_remove);
}

void binary_tree_remove_data(struct BinaryTree *node) {
    if(!node) return; //Nothing to do

    free(node->data);
    node->data = NULL;
    node->data_length = 0;
}

int binary_tree_add_data(struct BinaryTree *node, void *data, size_t data_length) {
    if (!data || data_length == 0) return 0; //Nothing to do

    //First get memory for copying, if it fails abort (do not touch old data)
    void *safe_copy = malloc(data_length);
    if (!safe_copy) {
        perror("[BinaryTree](add_data): Cannot allocate memory for data");
        return -1;
    }

    //Remove existing data
    if (node->data) {
        free(node->data);
    }

    //Store new data
    memcpy(safe_copy, data, data_length);
    node->data = safe_copy;
    node->data_length = data_length;

    return 0;
}

int binary_tree_create_and_insert(struct BinaryTree *root, int id, void *data, size_t data_length) {
    struct BinaryTree *new_link = binary_tree_create(id);
    if (!new_link) return -1;

    int error_set = binary_tree_add_data(new_link, data, data_length);
    if (error_set == -1) {
        binary_tree_remove(new_link, new_link->id); //We fake being the sole node in a tree
        return -1;
    }

    return binary_tree_insert(root, new_link);
}

int binary_tree_insert(struct BinaryTree *root, struct BinaryTree *new_link) {
    struct BinaryTree *search_node = root;

    while (search_node) {
        if (search_node->id == new_link->id) {
            perror("[BinaryTree](insert): Cannot insert duplicate node");
            return -1;
        }

        //Find unused spot
        if (search_node->id < new_link->id && search_node->upper) {
            search_node = search_node->upper;
        } else if (search_node->id > new_link->id && search_node->lower) {
            search_node = search_node->lower;

        } else {
            //Found an unused spot, Attach newly created node to tree
            new_link->parent = search_node;
            if (search_node->id < new_link->id) {
                search_node->upper = new_link;
            } else {
                search_node->lower = new_link;
            }
            return 0;
        }
    }

    perror("[BinaryTree](insert): Cannot insert after a NULL element");
    return -1;
}

struct BinaryTree *binary_tree_get(struct BinaryTree *root, int id) {
    struct BinaryTree *search_node = root;

    while (search_node) {
        if (search_node->id == id) return search_node;

        if (search_node->id < id) {
            search_node = search_node->upper;
        } else {
            search_node = search_node->lower;
        }
    }

    return NULL;
}

struct BinaryTree *binary_tree_find_root(struct BinaryTree *node) {
    if (!node) return NULL;

    struct BinaryTree *search_node = node;
    while (search_node->parent) search_node = search_node->parent;

    return search_node;
}

struct BinaryTree *binary_tree_find_minimum(struct BinaryTree *root) {
    if (!root) return NULL;

    struct BinaryTree *search_node = root;
    while (search_node->lower) search_node = search_node->lower;

    return search_node;
}

struct BinaryTree *binary_tree_find_maximum(struct BinaryTree *root) {
    if (!root) return NULL;

    struct BinaryTree *search_node = root;
    while (search_node->upper) search_node = search_node->upper;

    return search_node;
}

void binary_tree_log_node(struct BinaryTree *node) {
    if (!node) printf("[BinaryTree](log_node): nil\n");
    else {
        printf("[BinaryTree](log_node): id: %d", node->id);

        if (node->parent) printf(", parent: %d", node->parent->id);
        else printf(", parent: nil");

        if (node->lower) printf(", lower: %d", node->lower->id);
        else printf(", lower: nil");

        if (node->upper) printf(", upper: %d", node->upper->id);
        else printf(", upper: nil");

        printf("\n");
    }
}

void _binary_tree_log_helper(struct BinaryTree *node, int prefix_counter) {
    if (!node) return; //End of the tree

    //Create a string with a bunch of spaces for padding
    char prefix[prefix_counter * 2 + 1];
    for (int i = 0; i < prefix_counter * 2; ++i) {
        prefix[i] = ' ';
    }
    prefix[prefix_counter * 2] = 0;

    //Print spaces, pipe symbol and the actual id
    printf("%s└─%d\n", prefix, node->id);

    //Repeat for all nodes, recursively
    _binary_tree_log_helper(node->lower, prefix_counter + 1);
    _binary_tree_log_helper(node->upper, prefix_counter + 1);
}

void binary_tree_log(struct BinaryTree *root) {
    printf("[BinaryTree](log):\n");
    _binary_tree_log_helper(root, 0);
}