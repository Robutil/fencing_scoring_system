#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "linked_list.h"
#include "../../core/controller.h"

struct LinkedListNode {
    struct LinkedListNode *parent;
    struct LinkedListNode *child;

    void *data;
};

struct LinkedList {
    struct LinkedListNode *root;
    int is_circular;
    size_t size;
};

struct LinkedList *linked_list_create() {
    struct LinkedList *linked_list = malloc(sizeof(struct LinkedList));
    if (linked_list == NULL) return NULL;

    linked_list->root = NULL;
    linked_list->is_circular = 0;
    linked_list->size = 0;
    return linked_list;
}

struct LinkedListNode *linked_list_create_node(void *data) {
    /* Create container */
    struct LinkedListNode *link = malloc(sizeof(struct LinkedListNode));
    if (!link) {
        perror("[LinkedList](create): Cannot allocate memory for linked list container");
        return NULL;
    }

    /* Init fields */
    link->data = data;
    link->parent = NULL;
    link->child = NULL;
    return link;
}

void linked_list_destroy(struct LinkedList *list) {
    if (list->root == NULL) {
        free(list);
        return;
    }

    struct LinkedListNode *swap, *cur_child = list->root->child;
    while (cur_child) {
        swap = cur_child->child;
        linked_list_remove(list, cur_child);
        cur_child = swap;

        /* In case of a circular linked list we stop after a full circle */
        if (swap == list->root) {
            break;
        }
    }
    linked_list_remove(list, list->root);
    free(list);
}

int linked_list_insert_after(struct LinkedList *list, struct LinkedListNode *target_node, void *data) {
    if (list == NULL) return -1;

    struct LinkedListNode *new_node = linked_list_create_node(data);
    if (!new_node) return -1;

    /* If the given element had a child, inherit it */
    if (target_node->child != NULL) {
        new_node->child = target_node->child;
    }

    new_node->parent = target_node;
    target_node->child = new_node;

    if (list->is_circular && target_node->parent == target_node) {
        target_node->parent = new_node; /* Preserver circular */
    }
    list->size++;
    return 0;
}

int linked_list_insert_before(struct LinkedList *list, struct LinkedListNode *target_node, void *data) {
    if (list == NULL) return -1;

    struct LinkedListNode *new_node = linked_list_create_node(data);
    if (!new_node) return -1;

    /* If the given element had a parent, inherit it */
    if (target_node->parent != NULL) {
        new_node->parent = target_node->parent;
    }

    new_node->child = target_node;
    target_node->parent = new_node;

    if (list->is_circular && target_node->child == target_node) {
        target_node->child = new_node; /* Preserver circular */
    }
    list->size++;
    return 0;
}

struct LinkedListNode *linked_list_get_last(struct LinkedList *list) {
    if (list == NULL || list->root == NULL) return NULL;

    /* Keep check if a fixed point to avoid looping */
    struct LinkedListNode *avoid_loop_link = list->root->parent;

    struct LinkedListNode *last_element = list->root;
    while (last_element->child != NULL) {
        last_element = last_element->child;

        /* In case of a circular linked list, abort */
        if (avoid_loop_link == last_element) {
            return NULL;
        }
    }
    return last_element;
}

void linked_list_remove(struct LinkedList *list, struct LinkedListNode *node) {
    if (list == NULL || node == NULL) return;
    if (list->root == node) list->root = node->child;

    if (node->parent) node->parent->child = node->child;
    if (node->child) node->child->parent = node->parent;

    list->size--;
    free(node);
}

int linked_list_make_circular(struct LinkedList *list) {
    if (list == NULL || list->root == NULL || list->is_circular) return -1;
    list->is_circular = 1;

    struct LinkedListNode *start = linked_list_get_root(list);
    struct LinkedListNode *end = linked_list_get_last(list);

    start->parent = end;
    end->child = start;
    return 0;
}

static int _linked_list_push_prep_safe(struct LinkedList *list, void *data) {
    if (list && list->root == NULL) {
        list->root = linked_list_create_node(data);
        list->size++;
        return 1;
    }
    return 0;
}

int linked_list_push_back(struct LinkedList *list, void *data) {
    if (_linked_list_push_prep_safe(list, data) == 1) return list->root == NULL ? -1 : 0;
    return linked_list_insert_after(list, linked_list_get_last(list), data);
}

int linked_list_push_front(struct LinkedList *list, void *data) {
    if (_linked_list_push_prep_safe(list, data) == 1) return list->root == NULL ? -1 : 0;
    return linked_list_insert_before(list, linked_list_get_root(list), data);
}

struct LinkedListNode *linked_list_get_root(struct LinkedList *list) {
    return list == NULL ? NULL : list->root;
}

struct LinkedListNode *linked_list_next(struct LinkedListNode *node) {
    return node == NULL ? node : node->child;
}

struct LinkedListNode *linked_list_prev(struct LinkedListNode *node) {
    return node == NULL ? node : node->parent;
}

void *linked_list_get_data(struct LinkedListNode *node) {
    return node == NULL ? NULL : node->data;
}

unsigned long linked_list_size(struct LinkedList *list) {
    return list == NULL ? 0 : list->size;
}

struct LinkedListNode *linked_list_find(struct LinkedList *list, void *data) {
    struct LinkedListNode *cycle = linked_list_get_root(list);
    for (int i = 0; i < linked_list_size(list); ++i) {
        if (cycle->data == data) return cycle;
        cycle = linked_list_next(cycle);
    }
    return NULL;
}

void linked_list_foreach(struct LinkedList *list, int (*func)(struct LinkedListNode *)) {
    struct LinkedListNode *cycle = linked_list_get_root(list);
    for (int i = 0; i < linked_list_size(list); ++i) {
        if (func(cycle)) break;
        cycle = linked_list_next(cycle);
    }
}