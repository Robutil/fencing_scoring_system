#include <stdlib.h>
#include <stdio.h>

#include "linear_hash.h"

struct LinearHash {
    void **data;
    size_t size;
};

struct KeyContainer {
    int key;
    void *data;
};

struct LinearHash *linear_hash_create(size_t size) {
    struct LinearHash *linear_hash = malloc(sizeof(struct LinearHash));
    if (linear_hash == NULL) return NULL;
    linear_hash->size = size;

    linear_hash->data = calloc(size, sizeof(void *));
    if (linear_hash->data == NULL) {
        free(linear_hash);
        return NULL;
    }
    return linear_hash;
}

int linear_hash_destroy(struct LinearHash *linear_hash) {
    if (!linear_hash) return -1;
    free(linear_hash->data);
    free(linear_hash);
    return 0;
}

static size_t _calculate_hash(struct LinearHash *linear_hash, int key) {
    return (key % linear_hash->size);
}

int linear_hash_insert(struct LinearHash *linear_hash, int key, void *value) {
    struct KeyContainer *cont = malloc(sizeof(struct KeyContainer));
    if (cont == NULL) return -1;
    cont->data = value;
    cont->key = key;

    size_t index = _calculate_hash(linear_hash, key);
    for (; index < linear_hash->size; index++) {
        if (linear_hash->data[index] == NULL) {
            linear_hash->data[index] = cont;
            return 0;
        }
    }

    return -1;
}

long long _linear_hash_find_index(struct LinearHash *linear_hash, int key) {
    size_t index = _calculate_hash(linear_hash, key);
    for (struct KeyContainer *cont; index < linear_hash->size; index++) {
        if (linear_hash->data[index] != NULL) {
            cont = linear_hash->data[index];
            if (cont->key != key) continue;
            else return (int) index;
        }
    }
    return -1;
}

void *linear_hash_find(struct LinearHash *linear_hash, int key) {
    long long index = _linear_hash_find_index(linear_hash, key);
    return index == -1 ? NULL : ((struct KeyContainer *) linear_hash->data[index])->data;
}

void *linear_hash_pop(struct LinearHash *linear_hash, int key) {
    long long index = _linear_hash_find_index(linear_hash, key);
    if (index == -1) return NULL;

    struct KeyContainer *cont = linear_hash->data[index];
    void *save = cont->data;
    free(cont);
    linear_hash->data[index] = NULL;
    return save;
}