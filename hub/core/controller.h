#ifndef HUB_V2_CONTROLLER_H
#define HUB_V2_CONTROLLER_H

#include "../global.h"

struct GameState;

struct GameState *create_game_state();

void destroy_game_state(struct GameState *state);

void on_freq_detect(struct GameState *state, int player_index);

void on_hit_detect(struct GameState *state, int player_index);

void add_observer(struct GameState *state, ClientContainer *cont);

int remove_observer(struct GameState *state, ClientContainer *cont);

void remove_all_observers(struct GameState *state);

void on_tick(struct GameState *state);

#endif //HUB_V2_CONTROLLER_H
