#include <stdbool.h>
#include <zconf.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/time.h>
#include <string.h>

#include "light.h"
#include "../lib/buffer/linked_list.h"
#include "../lib/net/protocol.h"
#include "../global.h"

#define MAX_DELAY_FREQ_HIT_US 100000
#define SHOW_LIGHT_INTERVAL_US 3000000
#define MAX_DELAY_AFTER_SHOW_US 1000000

struct PlayerState {
    struct timespec freq_ts;
    struct timespec hit_ts;
    int light_state[2];
};

struct GameState {
    struct PlayerState players[2];
    struct LinkedList *observers;

    int global_active;
    struct timespec global_ts;
};

struct GameState *create_game_state() {
    struct GameState *state = malloc(sizeof(struct GameState));
    if (state == NULL) return NULL;
    memset(state, 0, sizeof(struct GameState));
    state->observers = linked_list_create();
    return state;
}

void destroy_game_state(struct GameState *state) {
    linked_list_destroy(state->observers);
    free(state);
}

void notify_observers(struct GameState *state, struct Message *msg) {
    printf("Notify observers");
    struct LinkedListNode *node = linked_list_get_root(state->observers);
    ClientContainer *cycle;
    while (node) {
        cycle = linked_list_get_data(node);
        protocol_send_message(cycle->socket, msg);

        node = linked_list_next(node);
        if (node == linked_list_get_root(state->observers)) {
            break;
        }
    }
}

void remove_all_observers(struct GameState *state) {
    struct LinkedListNode *save, *node = linked_list_get_root(state->observers);
    ClientContainer *cycle;
    while (node) {
        cycle = linked_list_get_data(node);
        cycle->game = NULL;

        save = node;
        node = linked_list_next(node);
        linked_list_remove(state->observers, save);
        if (node == linked_list_get_root(state->observers)) {
            break;
        }
    }
}

void send_light_packet(struct GameState *state, int light_id, int light_status) {
    char buf[2];
    buf[0] = 8;
    light_id |= (light_status << 7);
    buf[1] = (char) light_id;
    struct Message msg = {
            buf,
            2
    };
    notify_observers(state, &msg);
}

int add_observer(struct GameState *state, ClientContainer *cont) {
    return linked_list_push_back(state->observers, cont);
}

static int opposite_player(int player_index) {
    return player_index ^ 0x3;
}

u_int64_t timespec_diff_us(struct timespec *start, struct timespec *end) {
    return (u_int64_t) ((end->tv_sec - start->tv_sec) * 1000000 + (end->tv_nsec - start->tv_nsec) / 1000);
}

void reset_timespec(struct timespec *item) {
    item->tv_nsec = 0;
    item->tv_sec = 0;
}

int time_diff_in_range(struct timespec *start, struct timespec *end, u_int64_t diff_us) {
    if (!start || !end || (start->tv_sec == 0 && start->tv_nsec == 0) ||
        (end->tv_sec == 0 && end->tv_nsec == 0)) {
        return 0;
    }
    return timespec_diff_us(start, end) < diff_us;
}

void on_freq_detect(struct GameState *state, int player_index) {
    if (state == NULL) {
        printf("(on_freq_detect): Cannot change NULL state\n");
        return;
    }

    struct PlayerState *p1 = &state->players[player_index];
    struct PlayerState *p2 = &state->players[opposite_player(player_index)];

    clock_gettime(CLOCK_MONOTONIC_RAW, &(p1->freq_ts));
    if (!time_diff_in_range(&(p1->freq_ts), &(p2->hit_ts), MAX_DELAY_FREQ_HIT_US)) return;
    reset_timespec(&(p1->freq_ts));
    reset_timespec(&(p2->hit_ts));

    /* TODO: turn white light off */
    set_light(opposite_player(player_index), LIGHT_ON);
    send_light_packet(state, opposite_player(player_index) + 1, LIGHT_OFF); /* White light */
    send_light_packet(state, opposite_player(player_index), LIGHT_ON);
    if (!state->global_active) {
        state->global_active = 1;
        p2->light_state[0] = 1;
        clock_gettime(CLOCK_MONOTONIC_RAW, &(state->global_ts));
    }
}

void on_hit_detect(struct GameState *state, int player_index) {
    if (state == NULL) {
        printf("(on_hit_detect): Cannot change NULL state\n");
        return;
    }
    struct PlayerState *p1 = &(state->players[player_index]);
    struct PlayerState *p2 = &(state->players[opposite_player(player_index)]);

    clock_gettime(CLOCK_MONOTONIC_RAW, &(p1->hit_ts));
    if (state->global_active && time_diff_in_range(&(p1->hit_ts), &(state->global_ts), MAX_DELAY_AFTER_SHOW_US)) {
        /* Some light is already active */
        return;
    }

    if (p1->light_state[0] == 0 && time_diff_in_range(&(p1->hit_ts), &(p2->freq_ts), MAX_DELAY_FREQ_HIT_US)) {
        /* Opposite player has detected frequency, we have a point */
        reset_timespec(&(p1->hit_ts));
        reset_timespec(&(p2->freq_ts));
        set_light(player_index * 2, LIGHT_ON);
        p1->light_state[0] = 1;
        send_light_packet(state, player_index * 2, LIGHT_ON);

    } else if (p1->light_state[1] == 0) {
        /* White light */
        set_light(player_index * 2 + 1, LIGHT_ON);
        send_light_packet(state, player_index * 2 + 1, LIGHT_ON);
        p1->light_state[1] = 1;
    }

    if (!state->global_active) {
        state->global_active = 1;
        clock_gettime(CLOCK_MONOTONIC_RAW, &(state->global_ts));
    }
}

void on_tick(struct GameState *state) {
    struct timespec current_time;
    clock_gettime(CLOCK_MONOTONIC_RAW, &current_time);
    if (state->global_active && !time_diff_in_range(&(state->global_ts), &current_time, SHOW_LIGHT_INTERVAL_US)) {
        /* Lights are on but there is a timeout */
        state->global_active = 0;
        /* Reset lights */
        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 2; ++j) {
                if (!state->players[i].light_state[j]) continue;

                int light_id = i * 2 + j;
                state->players[i].light_state[j] = 0;
                set_light(light_id, LIGHT_OFF);
                send_light_packet(state, light_id, LIGHT_OFF);
            }
        }
    }
}

int remove_observer(struct GameState *state, ClientContainer *cont) {
    struct LinkedListNode *node = linked_list_get_root(state->observers);
    ClientContainer *cycle;
    while (node) {
        cycle = linked_list_get_data(node);
        if (cycle == cont) {
            linked_list_remove(state->observers, node);
            return 0;
        }
        node = linked_list_next(node);
        if (node == linked_list_get_root(state->observers)) {
            break;
        }
    }
    return 1;
}