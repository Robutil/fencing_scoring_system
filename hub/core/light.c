#include <stdio.h>
#include <stdlib.h>

#include "light.h"

int set_light(int id, int state) {
    if (EMULATE_HW) {
        printf("[Light](set_light): Request light %d state to %d\n", id, state);
        return 0;
    } else {
        /* TODO: implement this in hardware */
        perror("[Light](set_light): Not implemented!");
        exit(1);
    }
}
