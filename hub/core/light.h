#ifndef HUB_V2_LIGHT_H
#define HUB_V2_LIGHT_H

#define EMULATE_HW 1

#define LIGHT_ON 1
#define LIGHT_OFF 2

enum LightIdentifiers {
    player_1_primary = 0,
    player_1_secondary = 1,
    player_2_primary = 2,
    player_2_secondary = 3
};

int set_light(int id, int state);

#endif //HUB_V2_LIGHT_H
