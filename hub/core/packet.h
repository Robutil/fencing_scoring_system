#ifndef HUB_V2_PACKET_H
#define HUB_V2_PACKET_H

#include <stdint.h>
#include "../lib/net/message.h"

enum PacketCodes {                  /* PAYLOAD    | TYPE    */
    PACKET_PING = 1,                /* (optional) | char*   */
    PACKET_REGISTER_CLIENT,         /* none       | none    */
    PACKET_REGISTER_CLIENT_ACK,     /* success    | u_int_8 */
    PACKET_REGISTER_OBS,            /* none       | none    */
    PACKET_REGISTER_OBS_ACK,        /* success    | u_int_8 */
    PACKET_WEAPON_ACTIVE,           /* player_id  | u_int_8 */
    PACKET_FREQ_DETECT,             /* player_id  | u_int_8 */
    PACKET_LIGHT_CHANGE,             /* light_id   | u_int_8 */
    PACKET_GAMELIST_REQ,
    PACKET_GAMELIST_REP
};

struct Packet {
    uint8_t *id;
    void *data;
    size_t size;
};

struct Packet *parse_packet(struct Message *msg);

void free_packet(struct Packet *packet);

#endif //HUB_V2_PACKET_H
