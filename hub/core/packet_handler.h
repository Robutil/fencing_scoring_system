#ifndef HUB_V2_PACKET_H
#define HUB_V2_PACKET_H

#include <stdint.h>
#include <unistd.h>

#include "../global.h"
#include "../lib/net/message.h"

void handle_packet(State *state, ClientContainer *client, struct Message *msg);

struct LinkedListNode *find_game_node(State *state, const uint8_t *game_id);


#endif //HUB_V2_PACKET_H
