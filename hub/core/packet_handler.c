#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "packet_handler.h"
#include "controller.h"
#include "../lib/net/protocol.h"
#include "../lib/buffer/linked_list.h"
#include "../global.h"

enum PacketCodes {                  /* PAYLOAD    | TYPE    */
    PACKET_PING = 1,                /* (optional) | char*   */
    PACKET_REGISTER_CLIENT,         /* none       | none    */
    PACKET_REGISTER_CLIENT_ACK,     /* success    | u_int_8 */
    PACKET_REGISTER_OBS,            /* none       | none    */
    PACKET_REGISTER_OBS_ACK,        /* success    | u_int_8 */
    PACKET_WEAPON_ACTIVE,           /* player_id  | u_int_8 */
    PACKET_FREQ_DETECT,             /* player_id  | u_int_8 */
    PACKET_LIGHT_CHANGE,            /* light_id   | u_int_8 */
    PACKET_GAMELIST_REQ,
    PACKET_GAMELIST_REP,
    PACKET_OBS_WATCH_GAME_REQ,
    PACKET_OBS_WATCH_GAME_REP
};

void send_empty_packet(int socket, uint8_t packet_id) {
    char buf[1] = {packet_id};
    struct Message rep = {buf, 1};
    protocol_send_message(socket, &rep);
}

struct LinkedListNode *find_game_node(State *state, const uint8_t *game_id) {
    struct GameWrapper *wrapper = NULL;
    struct LinkedListNode *node = linked_list_get_root(state->games);
    while (node) {
        wrapper = linked_list_get_data(node);
        if (wrapper->game_id == *game_id) break;

        node = linked_list_next(node);
        if (node == linked_list_get_root(state->games)) {
            /* Circular linked list */
            node = NULL;
            break;
        }
    }
    return node;
}

struct Message *generate_gamelist_packet(State *state) {
    unsigned long amount_of_games = linked_list_size(state->games);
    if (amount_of_games == 0) {
        return NULL;
    }

    unsigned long packet_size = amount_of_games + 1;
    char *buffer = malloc(packet_size);
    buffer[0] = PACKET_GAMELIST_REP;

    struct LinkedListNode *cycle = linked_list_get_root(state->games);
    struct GameWrapper *wrapper;
    for (int i = 1; cycle != NULL; i++) {
        wrapper = linked_list_get_data(cycle);
        buffer[i] = (char) wrapper->game_id;

        cycle = linked_list_next(cycle);
        if (cycle == linked_list_get_root(state->games)) break;
    }
    struct Message *msg = malloc(sizeof(struct Message));
    msg->data = buffer;
    msg->size = packet_size;
    return msg;
}

struct GameWrapper *_find_game_wrapper(State *state, const uint8_t *game_id) {
    struct LinkedListNode *node = find_game_node(state, game_id);
    return node == NULL ? NULL : linked_list_get_data(node);
}

void handle_packet_ping(int socket, struct Message *msg) {
    /* Check for any str */
    if (msg->size == 0) printf("Received empty ping packet\n");
    else printf("Received ping packet: %.*s\n", (int) msg->size, (char *) msg->data);

    /* Send rep */
    char rep[] = " pong";
    rep[0] = PACKET_PING;
    struct Message outgoing_msg = {
            rep,
            strlen(rep)
    };
    protocol_send_message(socket, &outgoing_msg);
}

void handle_packet_register_client(State *state, ClientContainer *cont, struct Message *msg) {
    uint8_t status = 0;
    if (cont->level != UNKNOWN) {
        fprintf(stderr, "[PacketHandler](register_client): Role already registered for client [%d], is [%d]\n",
                cont->socket, cont->level);
        return;
    }
    if (msg->size < 2) {
        printf("[PacketHandler](register_client): Expected two bytes, got %lu. Aborting.\n", msg->size);
        return;
    }
    uint8_t *game_id = msg->data;
    struct GameWrapper *wrapper = _find_game_wrapper(state, msg->data);

    /* Game does not exist yet, so make one */
    if (wrapper == NULL) {
        printf("[PacketHandler](register_client): No game yet for game_id [%d]. Making new.\n", *game_id);
        struct GameState *game_state = create_game_state();
        if (game_state == NULL) {
            perror("Malloc GameState");
            return;
        }
        wrapper = calloc(sizeof(struct GameWrapper), 1);
        if (wrapper == NULL) {
            perror("Calloc GameWrapper");
            return;
        }
        wrapper->game_id = *game_id;
        wrapper->gs = game_state;
        if (linked_list_push_back(state->games, wrapper) == -1) {
            printf("[PacketHandler](register_client): Cannot push game to state\n");
            destroy_game_state(game_state);
            free(wrapper);
            return;
        }

        /* Notify all observers that there is a new game */
        struct Message *gamelist_msg = generate_gamelist_packet(state);
        struct LinkedListNode *cycle = linked_list_get_root(state->oberservers);
        for (int i = 0; i < linked_list_size(state->oberservers); ++i) {
            ClientContainer *c = linked_list_get_data(cycle);
            protocol_send_message(c->socket, gamelist_msg);
            cycle = linked_list_next(cycle);
        }
        protocol_destroy_message(gamelist_msg);
    }

    /* Add player to game */
    uint8_t *player_index = msg->data + 1;
    if (*player_index > 1) {
        printf("[PacketHandler](register_client): Cannot set player index [%d]\n", *player_index);
        return;
    }
    printf("[PacketHandler](register_client): Adding [%d] to game_id [%d] as player index [%d]\n", cont->socket,
           *game_id, *player_index);
    cont->level = CLIENT;
    if (wrapper->players[*player_index] != NULL) {
        /* There is already a player registered for our spot */
        printf("[PacketHandler](register_client): Kicking already registered player [%d]\n",
               wrapper->players[*player_index]->socket);
        wrapper->players[*player_index]->player_index = -1;
        wrapper->players[*player_index]->level = UNKNOWN;
        wrapper->players[*player_index]->game = NULL;
    }
    wrapper->players[*player_index] = cont;
    cont->game = wrapper;
    cont->player_index = *player_index;

    char data[2] = {PACKET_REGISTER_CLIENT_ACK, status};
    struct Message rep_msg = {data, 2};
    protocol_send_message(cont->socket, &rep_msg);
}

void handle_packet_observer_watch_game(State *state, ClientContainer *cont, struct Message *msg) {
    uint8_t status = 0;
    if (cont->level != OBSERVER) {
        status = 1;
        fprintf(stderr, "[PacketHandler](handle_packet_observer_watch_game):"
                        " Watch request for non observer from [%d], has role [%d]\n", cont->socket, cont->level);
    }
    if (msg->size == 1) {
        if (cont->game != NULL) {
            /* Remove observer from game that is currently being watched */
            remove_observer(cont->game->gs, cont);
        }
        struct GameWrapper *wrapper = _find_game_wrapper(state, msg->data);
        if (wrapper == NULL) {
            printf("[PacketHandler](handle_packet_observer_watch_game):"
                   " Cannot register for unknown game_id [%d]\n", *((uint8_t *) msg->data));
            status = 2;
        } else {
            printf("[PacketHandler](handle_packet_observer_watch_gamer): "
                   "Registering [%d] for game [%d]\n", cont->socket, *((uint8_t *) msg->data));
            cont->game = wrapper;
            add_observer(wrapper->gs, cont);
        }
    }

    char data[2] = {PACKET_OBS_WATCH_GAME_REP, status};
    struct Message rep_msg = {data, 2};
    protocol_send_message(cont->socket, &rep_msg);
}

void handle_packet_register_observer(State *state, ClientContainer *cont, struct Message *msg) {
    uint8_t status = 0;
    if (cont->level != UNKNOWN) {
        fprintf(stderr, "[PacketHandler](register_observer): Role already registered for [%d], is [%d]\n",
                cont->socket, cont->level);
        status = 1;
    } else {
        printf("[PacketHandler](register_observer): Registering [%d] as role observer\n", cont->socket);
        cont->level = OBSERVER;
        linked_list_push_back(state->oberservers, cont);
    }

    char data[2] = {PACKET_REGISTER_OBS_ACK, status};
    struct Message rep_msg = {data, 2};
    protocol_send_message(cont->socket, &rep_msg);
}

void handle_packet_request_gamelist(State *state, ClientContainer *cont) {
    struct Message *msg = generate_gamelist_packet(state);
    if (msg == NULL) {
        send_empty_packet(cont->socket, PACKET_GAMELIST_REP);
        return;
    }
    protocol_send_message(cont->socket, msg);
    protocol_destroy_message(msg);
}

void handle_packet(State *state, ClientContainer *client, struct Message *msg) {
    if (msg->size == 0) return; /* No packet id */

    /* First byte is packet id, rest is the packet contents */
    struct Message wrapper = {
            msg->data + 1,
            msg->size - 1
    };
    uint8_t *packet_id = msg->data;
    switch (*packet_id) {
        case PACKET_PING:
            handle_packet_ping(client->socket, &wrapper);
            break;

        case PACKET_REGISTER_CLIENT:
            handle_packet_register_client(state, client, &wrapper);
            break;

        case PACKET_REGISTER_OBS:
            handle_packet_register_observer(state, client, &wrapper);
            break;

        case PACKET_WEAPON_ACTIVE:
            if (client->level != CLIENT) {
                fprintf(stderr, "[Controller](handle_packet): GameState change requested by non-client: [%d]\n",
                        client->socket);
                break;
            }
            on_hit_detect(client->game->gs, client->player_index);
            break;

        case PACKET_FREQ_DETECT:
            if (client->level != CLIENT) {
                fprintf(stderr, "[Controller](handle_packet): GameState change requested by non-client: [%d]\n",
                        client->socket);
                break;
            }
            on_freq_detect(client->game->gs, client->player_index);
            break;

        case PACKET_GAMELIST_REQ:
            handle_packet_request_gamelist(state, client);
            break;

        case PACKET_OBS_WATCH_GAME_REQ:
            handle_packet_observer_watch_game(state, client, &wrapper);
            break;

        default:
            fprintf(stderr, "[Controller](handle_packet): Unknown packet id: [%d]\n", *packet_id);
    }
}