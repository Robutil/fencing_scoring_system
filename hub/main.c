#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "global.h"
#include "lib/net/server.h"
#include "lib/net/protocol.h"
#include "lib/buffer/linear_hash.h"
#include "lib/buffer/linear_buffer.h"
#include "core/packet_handler.h"
#include "lib/buffer/linked_list.h"
#include "core/controller.h"

State state;

void destroy_client_container(ClientContainer *cont) {
    if (cont == NULL) return;
    linear_buffer_destroy(cont->buffer);
    free(cont->name);
    free(cont);
}

void on_connect(int fd, struct sockaddr_in *info) {
    printf("(on_connect): New connection from: %d\n", info->sin_port);
    ClientContainer *cont = malloc(sizeof(ClientContainer));
    if (cont == NULL) {
        perror("Cannot allocate memory for client container");
        return;
    }
    cont->name = malloc(sizeof(struct sockaddr_in));
    if (cont->name == NULL) {
        perror("Cannot allocate memory for name in client container");
        free(cont);
        return;
    }
    memcpy(cont->name, info, sizeof(struct sockaddr_in));
    cont->buffer = linear_buffer_create(512);
    if (cont->buffer == NULL) {
        perror("Cannot allocate memory for msg buffer in client container");
        free(cont->name);
        free(cont);
        return;
    }
    cont->socket = fd;
    cont->level = UNKNOWN;
    cont->game = NULL;

    linear_hash_insert(state.connections, fd, cont);
}

int on_message(int fd) {
    printf("(on_message): Got message: called\n");
    ClientContainer *cont = linear_hash_find(state.connections, fd);
    if (cont == NULL) {
        fprintf(stderr, "(on_message): Cannot find client!");
        return -1;
    }

    ssize_t bytes_read = linear_buffer_read_fd(fd, cont->buffer);
    if (bytes_read <= 0) {
        /* Close will handle cleanup */
        return -1;
    }

    struct Message *msg;
    while ((msg = protocol_check_buffer(cont->buffer))) {
        printf("(on_message): New message, size: %lu\n", msg->size);
        handle_packet(&state, cont, msg);
        protocol_destroy_message(msg);
    }
    return 0;
}

void on_close(int fd) {
    printf("(on_close): Lost connection from: %d\n", fd);
    ClientContainer *cont = linear_hash_pop(state.connections, fd);
    if (cont == NULL) {
        fprintf(stderr, "(on_close): Cannot find client!");
        return;
    }
    if (cont->level == CLIENT) {
        printf("(on_close): Removing %d from game_id [%d] with player_index [%d]\n",
               cont->socket, cont->game->game_id, cont->player_index);
        cont->game->players[cont->player_index] = NULL;

        if (cont->game->players[cont->player_index ^ 1] == NULL) {
            /* No more players in the game */
            printf("(on_close): No more players in game_id [%d], removing it\n", cont->game->game_id);

            /* Remove all registered observers from this game */
            remove_all_observers(cont->game->gs);

            /* Remove game from list of active games */
            struct LinkedListNode *node = find_game_node(&state, (const uint8_t *) &cont->game->game_id);
            linked_list_remove(state.games, node);

            /* Clean up */
            struct GameWrapper *wrapper = linked_list_get_data(node);
            destroy_game_state(wrapper->gs);
            free(wrapper);
        }
    } else if (cont->level == OBSERVER) {
        if (cont->game != NULL) {
            remove_observer(cont->game->gs, cont);
        }
        linked_list_remove(state.oberservers, linked_list_find(state.oberservers, cont));
    }

    destroy_client_container(cont);
}

void on_tick_wrapper() {
    struct LinkedListNode *cycle = linked_list_get_root(state.games);
    struct GameWrapper *game_wrapper;
    while (cycle) {
        game_wrapper = linked_list_get_data(cycle);
        on_tick(game_wrapper->gs);

        cycle = linked_list_next(cycle);
        if (cycle == linked_list_get_root(state.games)) break;
    }
}

int main() {
    printf("[Main]: Starting server\n");
    state.connections = linear_hash_create(128);
    state.games = linked_list_create();
    state.oberservers = linked_list_create();

    struct ServerData *server_data = server_create(58080);
    server_set_on_connect(server_data, on_connect);
    server_set_on_message(server_data, on_message);
    server_set_on_close(server_data, on_close);
    server_set_on_tick(server_data, on_tick_wrapper);

    server_start(server_data, 1000);

    server_stop(server_data);
    server_destroy(server_data);

    linear_hash_destroy(state.connections);
    linked_list_destroy(state.games);
    return 0;
}