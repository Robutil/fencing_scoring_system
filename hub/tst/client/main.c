#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "../../lib/net/protocol.h"
#include "../../lib/net/client.h"

#define TST_GAME_ID 67
#define TST_GAME_PLAYER_INDEX 1
#define TST_EXPECTED_PACKETS 4 /* Set to -1 for infite */
#define WAIT_FOR_RESPONSE 1
#define VERBOSE 0


enum PacketCodes {                  /* PAYLOAD                                | TYPE    */
    PACKET_PING = 1,                /* (optional)                             | char*   */
    PACKET_REGISTER_CLIENT,         /* PACKET_ID, GAME_ID, PLAYER_INDEX       | u_int_8*    */
    PACKET_REGISTER_CLIENT_ACK,     /* success                                | u_int_8 */
    PACKET_REGISTER_OBS,            /* none                                   | none    */
    PACKET_REGISTER_OBS_ACK,        /* success                                | u_int_8 */
    PACKET_WEAPON_ACTIVE,           /* player_id                              | u_int_8 */
    PACKET_FREQ_DETECT,             /* player_id                              | u_int_8 */
    PACKET_LIGHT_CHANGE,            /* light_id                               | u_int_8 */
    PACKET_GAMELIST_REQ,
    PACKET_GAMELIST_REP
};

void send_empty_message(int conn_fd, uint8_t msg_id) {
    struct Message msg = {&msg_id, 1};
    protocol_send_message(conn_fd, &msg);
}

void send_msg_ping(int conn_fd, char *info) {
    if (info == NULL) info = "";

    /* Copy info message to ping packet buffer */
    const uint8_t packet_size = (uint8_t) (strlen(info) + 1);
    char output_buffer[packet_size];
    memcpy(&output_buffer[1], info, strlen(info));

    output_buffer[0] = PACKET_PING;
    struct Message msg = {
            output_buffer,
            packet_size
    };
    protocol_send_message(conn_fd, &msg);
}

void send_msg_register_client(int conn_fd) {
    char req_elev_buf[3] = {PACKET_REGISTER_CLIENT, TST_GAME_ID, TST_GAME_PLAYER_INDEX};
    struct Message req_client = {
            req_elev_buf,
            3
    };
    protocol_send_message(conn_fd, &req_client);
}

void send_msg_weapon_active(int conn_fd) {
    send_empty_message(conn_fd, PACKET_WEAPON_ACTIVE);
}

void send_msg_gamelist_req(int conn_fd) {
    send_empty_message(conn_fd, PACKET_GAMELIST_REQ);
}

void handle_packet_ping(int conn_fd, struct Message *msg) {
    if (msg->size == 0) {
        printf("[Main](handle_packet_ping): Got empty ping packet\n");
    } else {
        printf("[Main](handle_packet_ping): Got ping packet with contents: %.*s\n",
               (int) msg->size, (char *) msg->data);
    }
}

void handle_packet_register_client_ack(int conn_fd, struct Message *msg) {
    if (msg->size < 1) {
        fprintf(stderr,
                "[Main](handle_packet_register_client_ack): "
                "Expected byte response status not found in register response\n");
    } else {
        uint8_t *status = msg->data;
        if (*status == 0) {
            printf("[Main](handle_packet_register_client_ack): Registration as client complete\n");
        } else {
            printf("[Main](handle_packet_register_client_ack): "
                   "Unable to register as client, got response code: [%d]\n", *status);
        }
    }
}

void handle_packet_gamelist_rep(int conn_fd, struct Message *msg) {
    if (msg->size < 1) {
        printf("[Main](handle_packet_gamelist_rep): No games available on hub\n");
    } else {
        uint8_t *game_id;
        printf("[Main](handle_packet_gamelist_rep): Found the following games: [");
        for (int i = 0; i < msg->size; ++i) {
            game_id = msg->data + i;
            printf("%d", *game_id);
            if (i < msg->size - 1)printf(", ");
        }
        printf("]\n");
    }
}

void handle_new_packet(int conn_fd, struct Message *msg) {
    uint8_t *packet_id = msg->data;
    printf("[Main](handle_new_packet): Got new packet with id [%d] and length [%lu]\n", *packet_id, msg->size - 1);
    struct Message wrapper = {
            msg->data + 1,
            msg->size - 1
    };
    switch (*packet_id) {
        case PACKET_PING:
            handle_packet_ping(conn_fd, &wrapper);
            break;
        case PACKET_REGISTER_CLIENT_ACK:
            handle_packet_register_client_ack(conn_fd, &wrapper);
            break;

        case PACKET_GAMELIST_REP:
            handle_packet_gamelist_rep(conn_fd, &wrapper);
            break;

        default:
            fprintf(stderr, "[Main](handle_new_packet): Received unknown packet id [%d]\n", *packet_id);
    }
}

int main() {
    int conn_fd = connect_to_server("127.0.0.1", 58080);

    /* Send a ping to make sure server listens to us */
    send_msg_ping(conn_fd, "sample_string");

    /* Request: Register as client using TST_GAME variables */
    send_msg_register_client(conn_fd);

    /* Let hub know that we have hit something, should cause light change */
    send_msg_weapon_active(conn_fd);

    /* Request: Show current active games on hub. We expect to see at least our own game */
    send_msg_gamelist_req(conn_fd);

    size_t buffer_size = 64;
    struct LinearBuffer *input_buffer = linear_buffer_create(buffer_size);
    struct Message *msg = NULL;
    int packet_count = 0;
    while (WAIT_FOR_RESPONSE) {
        ssize_t bytes_read = linear_buffer_read_fd(conn_fd, input_buffer);
        if (bytes_read <= 0) {
            perror("Error reading from server");
            goto finish;
        }

        if (VERBOSE) printf("Read %d bytes\n", (int) bytes_read);

        while ((msg = protocol_check_buffer(input_buffer)) != NULL) {
            handle_new_packet(conn_fd, msg);
            protocol_destroy_message(msg);

            if (++packet_count == TST_EXPECTED_PACKETS) {
                goto finish;
            }
        }
    }

    finish:
    printf("[Main]: Stopping\n");
    close_server_connection(conn_fd);
    linear_buffer_destroy(input_buffer);
    printf("[Main]: Finished\n");
    return 0;
}