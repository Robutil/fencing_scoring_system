#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "../lib/buffer/linear_hash.h"

int main() {
    size_t size = 5;
    struct LinearHash *linear_hash = linear_hash_create(size);

    for (int i = 0; i < size; ++i) {
        char *buf = malloc(2);
        if (!buf) {
            perror("Cannot allocate memory for buffer");
            return EXIT_FAILURE;
        }
        buf[0] = (char) ('0' + i);
        buf[1] = 0;
        linear_hash_insert(linear_hash, i, buf);
        printf("Inserted: %s\n", (char *) linear_hash_find(linear_hash, i));
    }

    for (int j = 0; j < size; ++j) {
        char *buf = linear_hash_pop(linear_hash, j);
        printf("Found: %s\n", buf);
        free(buf);
        assert(linear_hash_pop(linear_hash, j) == NULL);
    }

    linear_hash_destroy(linear_hash);
}

