#ifndef HUB_V2_GLOBAL_H
#define HUB_V2_GLOBAL_H

#include <netinet/in.h>

#include "lib/buffer/linear_buffer.h"
#include "lib/buffer/linear_hash.h"

struct GameState;

enum ClientLevel {
    UNKNOWN,
    CLIENT,
    OBSERVER
};

typedef struct {
    struct LinearHash *connections;
    struct LinkedList *games;
    struct LinkedList *oberservers;
} State;

struct GameWrapper;

typedef struct {
    int socket;
    struct LinearBuffer *buffer;
    struct sockaddr_in *name;

    enum ClientLevel level;
    int player_index;
    struct GameWrapper *game;

} ClientContainer;

struct GameWrapper {
    int game_id;
    ClientContainer *players[2];
    struct GameState *gs;
};

#endif //HUB_V2_GLOBAL_H
